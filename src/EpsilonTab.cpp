#include "EpsilonTab.h"
#include "algorithm.basic.traversal/depthfirstsearch.h"
#include "graph.dyn/dynamicdigraph.h"
#include "graph/arc.h"
#include "graph/digraph.h"
#include "graph/graph_functional.h"
#include "graph/graphartifact.h"
#include "graph/vertex.h"
#include "observable.h"
#include "property/fastpropertymap.h"
#include <cmath>
#include <iostream>
#include <ostream>
#include <stdexcept>
#include <chrono>

/**
 * @brief Sets the epsilons for the two partitions that make up this class
 * 
 * @param epsilon epsilon for first epsilon partition
 * @param epsilon2 epsilon for the second epsilon partition
 */
bool EpsilonTab::setEpsilon(const double epsilon, const double epsilon2)
{
	if ((epsilon <= 0) || (epsilon >= 1))
	{
		std::cerr << "Epsilon must be between 0 and 1\n";
		return 0;
	}

	this->epsilon1 = epsilon;

	if ((epsilon2 <= 0) || (epsilon2 >= 1))
	{
		std::cerr << "Epsilon must be between 0 and 1\n";
		return 0;
	}

	this->epsilon2 = epsilon2;

	return 1;
}
void EpsilonTab::EpsTabOnVertexRemove(Algora::Vertex *v)
{
	HighDeg1.erase(v->getId());
	HighDeg2.erase(v->getId());
}
/**
 * @brief Modifies the epsilon partitions accoring to the removal of arc a
 * Notifies the coresponding observers if a vertex changes partition
 * 
 * @param a removed arc
 */
void EpsilonTab::EpsTabOnArcAdd(Algora::Arc *a)
{

	Algora::Vertex *head = a->getHead();
	Algora::Vertex *tail = a->getTail();
	this->nr_edges += 1;

	if (check_recompute(a, 1))
		return;

	observableArcAdd.notifyObservers(a);

	if (graph->getDiGraph()->getDegree(head, 0) >= 1.5 * theta1)
	{
		if (HighDeg1.insert({head->getId(), head}).second)
		{
			this->observableVertexUp1.notifyObservers(head);
		}
	}

	if (graph->getDiGraph()->getDegree(tail, 0) >= 1.5 * theta1)
	{
		if (HighDeg1.insert({tail->getId(), tail}).second)
		{
			this->observableVertexUp1.notifyObservers(tail);
		}
	}

	if (graph->getDiGraph()->getDegree(head, 0) >= 1.5 * theta2)
	{
		if (HighDeg2.insert({head->getId(), head}).second)
		{
			this->observableVertexUp2.notifyObservers(head);
		}
	}

	if (graph->getDiGraph()->getDegree(tail, 0) >= 1.5 * theta2)
	{
		if (HighDeg2.insert({tail->getId(), tail}).second)
		{
			this->observableVertexUp2.notifyObservers(tail);
		}
	}
}

/**
 * @brief Modifies the epsilon partitions accoring to the addition of arc a
 * Notifies the coresponding observers if a vertex changes partition
 * 
 * @param a added arc
 */
void EpsilonTab::EpsTabOnArcRemove(Algora::Arc *a)
{

	Algora::Vertex *head = (a->getHead());
	Algora::Vertex *tail = (a->getTail());
	this->nr_edges -= 1;

	if (check_recompute(a, 0))
		return;

	observableArcRemove.notifyObservers(a);

	if (graph->getDiGraph()->getDegree(head, 0) - 1 < 0.5 * theta1)
	{
		if (HighDeg1.erase(head->getId()))
		{


			this->observableVertexDown1.notifyObservers(head);
		}
	}
	if (graph->getDiGraph()->getDegree(tail, 0) - 1 < 0.5 * theta1)
	{
		if (HighDeg1.erase(tail->getId()))
		{

			this->observableVertexDown1.notifyObservers(tail);
		}
	}

	if (graph->getDiGraph()->getDegree(head, 0) - 1 < 0.5 * theta2)
	{
		if (HighDeg2.erase(head->getId()))
		{

			this->observableVertexDown2.notifyObservers(head);
		}
	}
	if (graph->getDiGraph()->getDegree(tail, 0) - 1 < 0.5 * theta2)
	{
		if (HighDeg2.erase(tail->getId()))
		{

			this->observableVertexDown2.notifyObservers(tail);
		}
	}
}

/**
 * @brief Prepares the object. This includes doing some precalculations and
 * linking with the observers of the graph given
 * Also starts and stops the time measurements of each step.
 * Mainatins the adjacency map that contains all presently active arcs
 * 
 */
bool EpsilonTab::prepare()
{
	if (epsilon1 == -1 || epsilon2 == -1)
	{
		std::cerr << "Must set epsilon before setting the Graph\n";
		return 0;
	}

	nr_edges = graph->getDiGraph()->getNumArcs(0);
	M = 2 * nr_edges;
	theta1 = std::pow(epsilon1, M);
	theta2 = std::pow(epsilon2, M);

	if (graph->getCurrentTime() != 0)
		if (!this->recompute(nullptr, 0))
		{
			std::cerr << "Error when creating Epsilon Table\n";
			return 0;
		}

	graph->getDiGraph()->onVertexRemove(&OnVertexRemoveId.emplace_back(0), [&](Algora::Vertex *v)
	{
		if (fixed)
			return;

		EpsTabOnVertexRemove(v);
	});
	graph->getDiGraph()->onArcAdd(&OnArcAddId.emplace_back(0), [&](Algora::Arc *a)
	{
		if (fixed)
			return;

		auto startTimeDyn = std::chrono::high_resolution_clock::now();
		insertedArcs[a] = 1;
		int head = a->getHead()->getId();
		int tail = a->getTail()->getId();

		//insert the new edge into the adjacency map
		std::pair<int, int> key;
		if (head < tail)
		{
			key = std::pair<int, int>({head, tail});
		}
		else
		{
			key = std::pair<int, int>({tail, head});
		}

		adjacencyMap[key] = true;

		EpsTabOnArcAdd(a);

		auto endTimeDyn = std::chrono::high_resolution_clock::now();
		currentDynTime = endTimeDyn - startTimeDyn;
	});

	graph->getDiGraph()->onArcRemove(&OnArcRemoveId.emplace_back(0), [&](Algora::Arc *a)
	{
		if (fixed)
			return;

		auto startTimeDyn = std::chrono::high_resolution_clock::now();

		insertedArcs.erase(a);
		int head = a->getHead()->getId();
		int tail = a->getTail()->getId();

		//reomve the edge from the adjacency map
		std::pair<int, int> key;
		if (head < tail)
		{
			key = std::pair<int, int>({head, tail});
		}

		else
		{
			key = std::pair<int, int>({tail, head});
		}

		adjacencyMap.erase(key);
		adjacencyMap.shrink_to_fit();

		EpsTabOnArcRemove(a);

		auto endTimeDyn = std::chrono::high_resolution_clock::now();
		currentDynTime = endTimeDyn - startTimeDyn;
	});

	return 1;
}

void EpsilonTab::cleanup()
{
	HighDeg1.clear();
	HighDeg2.clear();
}

/**
 * @brief Checks if the partition needs to be recomputed. If so, it starts the process
 * 
 * @param a edge that is currently being inserted/deleted
 * @param arcAdd if edge a is currenly added or removed
 * @return true if recomputation took place
 */
bool EpsilonTab::check_recompute(Algora::Arc *a, bool arcAdd)
{
	if ((nr_edges < std::floor(M * 0.25)) || (nr_edges > M))
	{
		recompute(a, arcAdd);
		return 1;
	}
	return 0;
}
/**
 * @brief Starts the recomputation. Removes and reinsertes all elemts from the epsilon partitions.
 * Then calls the function "on_recompute_func"
 * 
 * @param a edge that is currently being inserted/deleted
 * @param arcAdd if edge a is currenly added or removed
 * @return true if recomputation took place
 */
bool EpsilonTab::recompute(Algora::Arc *a, bool arcAdd)
{
	if (a != nullptr)
	{
		if (arcAdd)
			observableArcAdd.notifyObservers(a);
		else
			observableArcRemove.notifyObservers(a);
	}

	M = 2 * nr_edges;
	theta1 = std::pow(M, epsilon1);
	theta2 = std::pow(M, epsilon2);
	this->cleanup();

	graph->getDiGraph()->mapVertices([&](const Algora::Vertex *v)
	{
		if (graph->getDiGraph()->getDegree(v, true) > theta1)
		{
			HighDeg1.insert({v->getId(), v});
		}
		if (graph->getDiGraph()->getDegree(v, true) > theta2)
		{
			HighDeg2.insert({v->getId(), v});
		}
	});

	if (on_reompute_func != nullptr)
		on_reompute_func(arcAdd);

	return true;
}

void EpsilonTab::print_table()
{
	if (!hasGraph())
	{
		std::cerr << "No Graph set\n";
		return;
	}
	std::cout << "First High Degree Table: \n";

	for (auto v : HighDeg1)
		std::cout << v.first << " ";

	std::cout << "\n\nSecond High Degree Table: \n";

	for (auto v : HighDeg2)
		std::cout << v.first << " ";

	/*
	std::cout << "Low Degree: \n";


	graph->getDiGraph()->mapVertices([&](const Algora::Vertex *v)
	{
		if (HighDeg.count(v->getId()) == 0)
			std::cout << v->getId() << " ";
		return 1;

	});

	std::cout << "\n\n";
	*/
}
/**
 * @brief Adds a function to be called when a vertex moves to high degree in the first epsilon partition
 * 
 * @param id id of the observer
 * @param vvFun function to call
 */
void EpsilonTab::onVertexToHigh1(void *id, const Algora::VertexMapping &vvFun)
{
	this->observableVertexUp1.addObserver(id, vvFun);
}
/**
 * @brief Adds a function to be called when a vertex moves to high degree in the second epsilon partition
 * 
 * @param id id of the observer
 * @param vvFun function to call
 */
void EpsilonTab::onVertexToHigh2(void *id, const Algora::VertexMapping &vvFun)
{
	this->observableVertexUp2.addObserver(id, vvFun);
}

/**
 * @brief Adds a function to be called when a vertex moves to low degree in the first epsilon partition
 * 
 * @param id id of the observer
 * @param vvFun function to call
 */
void EpsilonTab::onVertexToLow1(void *id, const Algora::VertexMapping &vvFun)
{
	this->observableVertexDown1.addObserver(id, vvFun);
}
/**
 * @brief Adds a function to be called when a vertex moves to low degree in the second epsilon partition
 * 
 * @param id id of the observer
 * @param vvFun function to call
 */
void EpsilonTab::onVertexToLow2(void *id, const Algora::VertexMapping &vvFun)
{
	this->observableVertexDown2.addObserver(id, vvFun);
}

/**
 * @brief Adds a function to be called when an arc is added to the graph
 * 
 * @param id id of the observer
 * @param vvFun function to call
 */
void EpsilonTab::onArcAdd(void *id, const Algora::ArcMapping &vvFun)
{
	this->observableArcAdd.addObserver(id, vvFun);
}
/**
 * @brief Adds a function to be called when an arc is removed from the graph
 * 
 * @param id id of the observer
 * @param vvFun function to call
 */
void EpsilonTab::onArcRemove(void *id, const Algora::ArcMapping &vvFun)
{
	this->observableArcRemove.addObserver(id, vvFun);
}

/**
 * @brief Removes a function to be called when a vertex moves to high degree in the first epsilon partition
 * 
 * @param id id of the observer
 */
void EpsilonTab::removeOnVertexToHigh1(void *id)
{
	this->observableVertexUp1.removeObserver(id);
}
/**
 * @brief Removes a function to be called when a vertex moves to high degree in the second epsilon partition
 * 
 * @param id id of the observer
 */
void EpsilonTab::removeOnVertexToHigh2(void *id)
{
	this->observableVertexUp2.removeObserver(id);
}

/**
 * @brief Removes a function to be called when a vertex moves to low degree in the first epsilon partition
 * 
 * @param id id of the observer
 */
void EpsilonTab::removeOnVertexToLow1(void *id)
{
	this->observableVertexDown1.removeObserver(id);
}
/**
 * @brief Removes a function to be called when a vertex moves to low degree in the second epsilon partition
 * 
 * @param id id of the observer
 */
void EpsilonTab::removeOnVertexToLow2(void *id)
{
	this->observableVertexDown2.removeObserver(id);
}

/**
 * @brief Removes a function to be called when an arc is added to the graph
 * 
 * @param id id of the observer
 */
void EpsilonTab::removeOnArcAdd(void *id)
{
	this->observableArcAdd.removeObserver(id);
}
/**
 * @brief Removes a function to be called when an arc is removed from the graph
 * 
 * @param id id of the observer
 */
void EpsilonTab::removeOnArcRemove(void *id)
{
	this->observableArcRemove.removeObserver(id);
}
