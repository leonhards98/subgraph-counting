#include "StructureCounts.h"

/**
 * @brief updates the uHv-auxiliary count by calculating the number the count should change by when inserting/removing the given arc. 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the arc is added or deleted
 * 
 * @param a arc that is added/deleted
 * @param operation function that either increments or decrements the a given map. This should decrement if the arc is removed and increment if 
 * the arc in inserted
 * @param recompute_after_removed set to true if this operation is part of a recomputation after an arc was removed
 */
void StructureCounts::uHvArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();

	bool head_high = EpsTab.is_high_deg(head);
	bool tail_high = EpsTab.is_high_deg(tail);

	if (head_high && tail_high)
	{
		for (auto h = EpsTab.getEpsbegin(); h != EpsTab.getEpsend(); h++)
		{
			if (h->second == head || h->second == tail)
				continue;

			if (EpsTab.arcExists(head, h->second))
			{
				operation(uHv, h->first, tail->getId(), 1);
			}

			if (EpsTab.arcExists(tail, h->second))
			{
				operation(uHv, head->getId(), h->first, 1);
			}
		}
	}
}

/**
 * @brief updates the uHv-auxiliary count by calculating the number the count should change by when the given vertex changes from one epsilon partition to the other 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the vertex changes from low to high or the other way around
 * 
 * @param v vertex that changes partition
 * @param operation function that either increments or decrements the a given map. This should decrement if the vertex becomes high degree and increment if 
 * the vertex becomes low degree
 */
void StructureCounts::uHvVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int))
{
	bool done{false};

	graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a1)
	{
		auto x = a1->getOther(v);
		if (EpsTab.is_low_deg(x))
			return;
		
		done = false;
		graph->getDiGraph()->mapIncidentArcsUntil(v,[&](const Algora::Arc *a2)
		{

			auto y = a2->getOther(v);

			if (EpsTab.is_low_deg(y))
				return;
			
			operation(uHv,x->getId(),y->getId(),1);

		},[&done,&a1](const Algora::Arc* a_now) 
		{
			if (done == false)
				done = (a1 == a_now);
			return done;
		}); 
	});

	if (EpsTab.is_high_deg(v))
	{
		graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a1)
		{
			auto w = a1->getOther(v);
			if (EpsTab.is_low_deg(w))
				return;
			for (auto h = EpsTab.getEpsbegin(); h != EpsTab.getEpsend(); h++)
			{
				if (EpsTab.arcExists(w,h->second))
					operation(uHv,h->first,v->getId(),1);
			} 
		});
	}
	else
	{
		for (auto h = EpsTab.getEpsbegin(); h != EpsTab.getEpsend(); h++)
		{
			operation(uHv, h->first, v->getId(), INT_MAX);
		}
	}
}
