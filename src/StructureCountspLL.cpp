#include "StructureCounts.h"

/**
 * @brief updates the pLL-auxiliary count by calculating the number the count should change by when inserting/removing the given arc. 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the arc is added or deleted
 * 
 * @param a arc that is added/deleted
 * @param operation function that either increments or decrements the a given map. This should decrement if the arc is removed and increment if 
 * the arc in inserted
 */
void StructureCounts::pLLArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	bool head_low{EpsTab.is_low_deg(head)};
	bool tail_low{EpsTab.is_low_deg(tail)};
	bool x1_low, x2_low, Exv, Exu;

	if (head_low)
	{
		graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *a1)
		{
			auto a1_other = a1->getOther(head);
			if (a1_other == tail)
				return;
			if ( EpsTab.is_low_deg(a1_other))
				x1_low = true;
			else
				x1_low = false;

			if (!EpsTab.arcExists(tail,a1_other))
				Exv = false;
			else
				Exv = true;

			graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a2)
			{
				auto a2_other = a2->getOther(head);
				if (a2_other == tail || a2_other == a1_other)
					return;

				if (x1_low && EpsTab.arcExists(a1_other,a2_other))
				{
					operation(pLL,tail->getId(),a2_other->getId(),1);
				}

				if (!Exv)
					return;

				if (x1_low)
				{
					operation(pLL, tail->getId(),a2_other->getId(),1);
				}

				if (tail_low)
				{
					operation(pLL,a1_other->getId(),a2_other->getId(),1);
				}


			});

			if (!Exv)
				return;
			if (!x1_low)
				return;

			graph->getDiGraph()->mapIncidentArcs(a1_other,[&](const Algora::Arc *a3)
			{
				auto a3_other = a3->getOther(a1_other);

				if (a3_other == head || a3_other == tail)
					return;


				operation(pLL,tail->getId(),a3_other->getId(),1);
			}); });
	}

	if (tail_low)
	{
		graph->getDiGraph()->mapIncidentArcs(tail, [&](const Algora::Arc *a1)
		{
			auto a1_other = a1->getOther(tail);
			if (a1_other == head)
				return;
			if ( EpsTab.is_low_deg(a1_other))
				x2_low = true;
			else
				x2_low = false;

			if (!EpsTab.arcExists(head,a1_other))
				Exu = false;
			else
				Exu = true;


			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a2)
			{
				auto a2_other = a2->getOther(tail);
				if (a2_other == head || a2_other == a1_other)
					return;

				if (x2_low && EpsTab.arcExists(a1_other,a2_other))
				{
					operation(pLL,head->getId(),a2_other->getId(),1);
				}

				if (!Exu)
					return;

				if (x2_low)
				{
					operation(pLL, head->getId(),a2_other->getId(),1);
				}

				if (head_low)
				{
					operation(pLL,a1_other->getId(),a2_other->getId(),1);
				}


			});

			if (!Exu)
				return;
			if (!x2_low)
				return;

			graph->getDiGraph()->mapIncidentArcs(a1_other,[&](const Algora::Arc *a3)
			{
				auto a3_other = a3->getOther(a1_other);

				if (a3_other == head || a3_other == tail)
					return;

				operation(pLL,head->getId(),a3_other->getId(),1);
			}); 
		});
	}
}

/**
 * @brief updates the pLL-auxiliary count by calculating the number the count should change by when the given vertex changes from one epsilon partition to the other 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the vertex changes from low to high or the other way around
 * 
 * @param v vertex that changes partition
 * @param operation function that either increments or decrements the a given map. This should decrement if the vertex becomes high degree and increment if 
 * the vertex becomes low degree
 */
void StructureCounts::pLLVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int))
{

	graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a1)
										 {
		auto y = a1->getOther(v);

		
		graph->getDiGraph()->mapIncidentArcs(v,[&](const Algora::Arc *a2)
		{
			auto z = a2->getOther(v);

			if (EpsTab.arcExists(y,z) && EpsTab.is_low_deg(z))
			{
				graph->getDiGraph()->mapIncidentArcs(z,[&](const Algora::Arc *a3)
				{
					auto x = a3->getOther(z);
					if (x == v || x == y)
						return;
					//std::cout << "Nr 10 ( " << y->getId() << " " << x->getId() << " ): 1"<< "\n";
					operation(pLL,y->getId(),x->getId(),1);
				});
			}

			if (a1->getId() <= a2->getId())
				return;
			
			graph->getDiGraph()->mapIncidentArcs(v,[&](const Algora::Arc *a3)
			{
				auto x = a3->getOther(v);

				if (x == y || x == z || EpsTab.is_high_deg(x))
					return;
				
				if (EpsTab.arcExists(x,y))
				{
					//std::cout << "Nr 11 ( " << y->getId() << " " << z->getId() << " ): 1"<< "\n";

					operation(pLL,y->getId(),z->getId(),1);
				}
				if (EpsTab.arcExists(x,z))
				{
					//std::cout << "Nr 12 ( " << y->getId() << " " << z->getId() << " ): 1"<< "\n";

					operation(pLL,y->getId(),z->getId(),1);
				}
				
			});
				

		});
	});
}
