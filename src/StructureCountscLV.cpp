#include "StructureCounts.h"
#include "graph/vertex.h"

/**
 * @brief updates the cLV-auxiliary count by calculating the number the count should change by when inserting/removing the given arc. 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the arc is added or deleted
 * 
 * @param a arc that is added/deleted
 * @param operation function that either increments or decrements the a given map. This should decrement if the arc is removed and increment if 
 * the arc in inserted
 * @param recompute_after_removed set to true if this operation is part of a recomputation after an arc was removed
 */
void StructureCounts::cLVArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int), bool recompute_after_removed)
{

	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();

	int deg_t{0}, deg_h{0};
	bool done{false};

	if (recompute_after_removed)
	{
		graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *)
		{ deg_h++; });

		graph->getDiGraph()->mapIncidentArcs(tail, [&](const Algora::Arc *)
		{ deg_t++; });
	}
	else
	{
		deg_h = graph->getDiGraph()->getDegree(head, 0);
		deg_t = graph->getDiGraph()->getDegree(tail, 0);
	}

	if (EpsTab.is_low_deg(head))
	{
		graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(head);
			if (x == tail)
				return;
			done = false;
			graph->getDiGraph()->mapIncidentArcsUntil(head,[&](const Algora::Arc *a2)
			{
				auto y = a2->getOther(head);
				if (y == tail)
					return;

				operation(cLV,x->getId(),y->getId(),1);

			},[&done,&a1](const Algora::Arc* a_now) 
			{
				if (done == false)
					done = (a1 == a_now);
				return done;
			}); 
		});

		int increment = deg_h - 2;
		if (increment > 0)
		{

			graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *a_head)
			{
				auto other = a_head->getOther(head);

				if (other == tail)
					return;

				operation(cLV,tail->getId(),other->getId(),increment); 
			});
		}
	}

	if (EpsTab.is_low_deg(tail))
	{
		graph->getDiGraph()->mapIncidentArcs(tail, [&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(tail);
			if (x == head)
				return;
			done = false;
			graph->getDiGraph()->mapIncidentArcsUntil(tail,[&](const Algora::Arc *a2)
			{
				auto y = a2->getOther(tail);
				if (y == head)
					return;

				operation(cLV,x->getId(),y->getId(),1);
			},[&done,&a1](const Algora::Arc* a_now) 
			{
				if (done == false)
					done = (a1 == a_now);
				return done;
			}); 
		});

		int increment = deg_t - 2;
		if (increment > 0)
		{

			graph->getDiGraph()->mapIncidentArcs(tail, [&](const Algora::Arc *a_tail)
			{
				auto other = a_tail->getOther(tail);

				if (other == head)
					return;

				operation(cLV,head->getId(),other->getId(),increment);
			});
		}
	}
}

/**
 * @brief updates the cLV-auxiliary count by calculating the number the count should change by when the given vertex changes from one epsilon partition to the other 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the vertex changes from low to high or the other way around
 * 
 * @param v vertex that changes partition
 * @param operation function that either increments or decrements the a given map. This should decrement if the vertex becomes high degree and increment if 
 * the vertex becomes low degree
 */
void StructureCounts::cLVVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int))
{
	int deg_v{0};
	bool done{false};
	graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *)
	{ deg_v++; });

	int increment = deg_v - 2;

	graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a1)
	{
		auto x = a1->getOther(v);
		
		done = false;
		graph->getDiGraph()->mapIncidentArcsUntil(v,[&](const Algora::Arc *a2)
		{
			auto y = a2->getOther(v);
			operation(cLV,x->getId(),y->getId(),increment);

		},[&done,&a1](const Algora::Arc* a_now) 
		{
			if (done == false)
				done = (a1 == a_now);
			return done;
		}); 
	});
}
