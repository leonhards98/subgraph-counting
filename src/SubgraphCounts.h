#include "StructureCounts.h"
#include "flat_hash_map/bytell_hash_map.hpp"
#include "graph.dyn/dynamicdigraph.h"
#include "graph.incidencelist/incidencelistvertex.h"
#include "graph/vertex.h"
#include <boost/unordered/unordered_map_fwd.hpp>
#include <chrono>
#include <string>
#include <vector>

/**
 * @brief Counts the number of total and s subgraphs in the given graph
 * 
 */
class SubgraphCounts
{

private:
	Algora::DynamicDiGraph *graph;
	StructureCounts StrucCount;
	int diamond{0};
	int tPath{0};
	int paw{0};
	int fCycle{0};
	int fClique{0};
	int claw{0};
    int tCycle{0};
	ska::bytell_hash_map<int, int> sTriangle;
	ska::bytell_hash_map<int, int> sTriangle_save;
	ska::bytell_hash_map<int, int> sTPath;
	ska::bytell_hash_map<int, int> sClaw;
	ska::bytell_hash_map<std::pair<int, int>, int> sClaw2;
	ska::bytell_hash_map<int, int> sPaw;
	ska::bytell_hash_map<int, int> sFCycle;
	ska::bytell_hash_map<int, int> sDiamond;
	ska::bytell_hash_map<int, int> sFClique;

	bool objectives[9] {false};

	double epsilon1{-1};
	double epsilon2{-1};
	std::vector<Algora::Vertex *> CurrentObservedVertices;
	ska::bytell_hash_map<int, bool> observedVertices;
	ska::bytell_hash_map<int, bool> already_observed;


	void DiamondArcChange(const Algora::Arc *v, void(operation)(int &, int));
	void tPathArcChange(const Algora::Arc *v, void(operation)(int &, int));
	void pawArcChange(const Algora::Arc *v, void(operation)(int &, int));
	void fCycleArcChange(const Algora::Arc *v, void(operation)(int &, int));
	void fCliqueArcChange(const Algora::Arc *a, void(operation)(int &, int));
	void ClawArcChange(const Algora::Arc *a, void(operation)(int &, int));

	void sTriangleArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int));
	void sTPathArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int));
	void sClawArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int));
	int sPawArcWithoutEdge(const Algora::Vertex *u, const Algora::Vertex *v, const Algora::Vertex *e1, const Algora::Vertex *e2);
	int sPawArcWithEdge(const Algora::Vertex *u, const Algora::Vertex *v, const Algora::Vertex *e1, const Algora::Vertex *e2, bool compensate_uLv);
	void sPawArcChange(Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int));
	void sFCycleArcChange(Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int));
	void sDiamondArcChange(Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int));
	void sFCliqueArcChange(Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int));

	int sTriangleCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s);
	int sTPathCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s);
	int sClawCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s, int deg_h, int deg_t);
	int sPawCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s, bool arc_removes, int tHead, int tTail, int tHeadTail);
	int sFCycleCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s);
	int sDiamondCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s);
	int sFCliqueCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s);

	/**
	 * @brief Adds to a counter.
	 * Ignores counts that are <= 0
	 * 
	 * @param counter counter to add to
	 * @param increment number that is added
	 */
	static void add(int &counter, int increment)
	{
		if (increment <= 0)
		{
			return;
		}
		counter += increment;
	}
	/**
	 * @brief Subtracts from a counter.
	 * The counter is not allowed to be < 0
	 * 
	 * @param counter counter to add to
	 * @param increment number that is added
	 */
	static void subtract(int &counter, int decrement)
	{
		if (decrement <= 0)
		{
			return;
		}
		if (decrement >= counter)
		{
			counter = 0;
			return;
		}
		counter -= decrement;
	}
	/**
	 * @brief Increases the value of the element of the given hash map defined by the given key. If the key doesn't exist, create a new entry.
	 * 
	 * @param map hash map to delete from
	 * @param key key to the element that is reduced
	 * @param increment number by which to increase
	 */
	static void increaseOrCreate(ska::bytell_hash_map<int, int> &map, int key, int increment)
	{
		if (increment <= 0)
			return;

		map[key] += increment;
	}

	/**
	 * @brief Reduces the value of the element of the given hash map defined by the given key. 
	 * If the decrement is larger than the value, deletes the entry.
	 * 
	 * @param map hash map to delete from
	 * @param key key to the element that is reduced
	 * @param decrement number by which to decrease
	 */
	static void reduceOrDelete(ska::bytell_hash_map<int, int> &map, int key, int decrement)
	{
		if (decrement < 1)
			return;
		auto it = map.find(key);
		if (it == map.end())
			return;
		if (it->second <= decrement)
		{
			map.erase(it);
			map.shrink_to_fit();
		}
		else
			it->second -= decrement;
	}

	/**
	 * @brief Get the number of triangles that vertex v was partof last step
	 * 
	 * @param v 
	 * @return int 
	 */
	int getNrTriangles_save(const Algora::Vertex *v)
	{
		auto it = sTriangle_save.find(v->getId());
		return (it == sTriangle_save.end() ? 0 : it->second);
	}

public:
	/**
	 * @brief Get the time that was needed for the last operation
	 * 
	 * @return std::chrono::duration<double> 
	 */
	std::chrono::duration<double> getCurrentDynTime()
	{
		return StrucCount.EpsTab.getCurrentDynTime();
	}
	void setEpsilon(double epsilon1, double epsilon2);
	void setObjectives(std::vector<std::string> &observedVertices, bool tCycles, bool tPaths, bool claws, bool paws, bool fCycles, bool diamonds, bool fCliques, bool total_counts, bool s_counts);
	bool prepare();

	SubgraphCounts() : graph(nullptr) {}
	void setGraph(Algora::DynamicDiGraph *graph)
	{
		this->graph = graph;
	}
	bool hasGraph() { return graph != nullptr; };

    void tCycleArcChange(const Algora::Arc *a, void(operation)(int &, int));

	/**
	 * @brief Get the total number of three-cycles.
	 * 
	 * @return int 
	 */
	int getNrtCycle()
	{
		return tCycle;
	}

	/**
	 * @brief Get the total number of diamonds.
	 * 
	 * @return int 
	 */
	int getNrDiamonds()
	{
		return diamond;
	}
	int getNrTPaths_extended(const Algora::Vertex *u, const Algora::Vertex *v);
	/**
	 * @brief Get the total number of three-paths.
	 * 
	 * @return int 
	 */
	int getNrTPaths()
	{
		return tPath;
	}
	int getNrTriangle_extended(Algora::IncidenceListVertex *u, Algora::IncidenceListVertex *v);
	int getNrTriangle_extended_with_edge(const Algora::Vertex *u, const Algora::Vertex *v, const Algora::Vertex *e1, const Algora::Vertex *e2);
	int getNrTriangle_extended(const Algora::Vertex *v);
	int getNrTriangle_extended_with_edge(const Algora::Vertex *v, const Algora::Vertex *e1, const Algora::Vertex *e2);
	int getNrPaws_extended(const Algora::Vertex *head, const Algora::Vertex *tail, int tHead, int tTail, int tHeadTail);
	/**
	 * @brief Get the total number of paws.
	 * 
	 * @return int 
	 */
	int getNrPaws()
	{
		return paw;
	}
	int getNrfCycles_extended(const Algora::Vertex *u, const Algora::Vertex *v);
	/**
	 * @brief Get the total number of four-cycles.
	 * 
	 * @return int 
	 */
	int getNrfClycles()
	{
		return fCycle;
	}

	StructureCounts *getStructureCount()
	{
		return &StrucCount;
	}

	/**
	 * @brief Get the s-count of triangels for the vertex v
	 * 
	 * @param v 
	 * @return int 
	 */
	int getNrTriangles(const Algora::Vertex *v)
	{
		auto it = sTriangle.find(v->getId());
		return (it == sTriangle.end() ? 0 : it->second);
	}

	/**
	 * @brief Get the s-count of three-paths for the vertex v
	 * 
	 * @param v 
	 * @return int 
	 */
	int getNrTPaths(const Algora::Vertex *v)
	{
		auto it = sTPath.find(v->getId());
		return (it == sTPath.end() ? 0 : it->second);
	}
	/**
	 * @brief Get the total number of claws.
	 * 
	 * @return int 
	 */
	int getNrClaws()
	{
		return claw;
	}
	int getNrClaws_extended(const Algora::Vertex *head, const Algora::Vertex *tail);
	/**
	 * @brief Get the s-count of claws for the vertex v
	 * 
	 * @param v 
	 * @return int 
	 */
	int getNrClaws(const Algora::Vertex *v)
	{
		auto it = sClaw.find(v->getId());
		return (it == sClaw.end() ? 0 : it->second);
	}
	int getNrClws_extended(const Algora::Vertex *u, const Algora::Vertex *v);

	/**
	 * @brief Get the s-count of paws for the vertex v
	 * 
	 * @param v 
	 * @return int 
	 */
	int getNrPaws(const Algora::Vertex *v)
	{
		auto it = sPaw.find(v->getId());
		return (it == sPaw.end() ? 0 : it->second);
	}

	/**
	 * @brief Get the s-count of four-cycles for the vertex v
	 * 
	 * @param v 
	 * @return int 
	 */
	int getNrfClycles(const Algora::Vertex *v)
	{
		auto it = sFCycle.find(v->getId());
		return (it == sFCycle.end() ? 0 : it->second);
	}

	int getNrDiamonds_extended(const Algora::Vertex *u, const Algora::Vertex *v);
	/**
	 * @brief Get the s-count of diamonds for the vertex v
	 * 
	 * @param v 
	 * @return int 
	 */
	int getNrDiamonds(const Algora::Vertex *v)
	{
		auto it = sDiamond.find(v->getId());
		return (it == sDiamond.end() ? 0 : it->second);
	}
	/**
	 * @brief Get the total number of four-cliques.
	 * 
	 * @return int 
	 */
	int getNrFCliques()
	{
		return fClique;
	}
	int getNrFCliques_extended(const Algora::Vertex *u, const Algora::Vertex *v);
	/**
	 * @brief Get the s-count of four-cliques for the vertex v
	 * 
	 * @param v 
	 * @return int 
	 */
	int getNrFCliques(const Algora::Vertex *v)
	{
		auto it = sFClique.find(v->getId());
		return (it == sFClique.end() ? 0 : it->second);
	}
};
