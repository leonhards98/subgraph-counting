/**
 * This file contains all functions that count structures that update the s-counts of a given vertex when an edge is inserted/deleted
 * 
 */

#include "SubgraphCounts.h"

/**
 * @brief Gives the change of the s-count regarding the subgraph triangle for the vertex s when an edge that is given by two vertices is inserted/deleted.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @param s vertex for which the change in s-count should be updated
 * @return int 
 */
int SubgraphCounts::sTriangleCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s)
{
	int increment{0};
	if (s != head && s != tail)
	{
		if (StrucCount.EpsTab.arcExists(head, s) && StrucCount.EpsTab.arcExists(tail, s))
		{
			increaseOrCreate(sTriangle_save, s->getId(), this->getNrTriangles(s));
			increment++;
		}
	}
	else if (s == tail)
	{
		increaseOrCreate(sTriangle_save, s->getId(), this->getNrTriangles(s));
		increment += this->getNrTriangle_extended(graph->getDiGraph()->vertexAt(s->getId()), graph->getDiGraph()->vertexAt(head->getId()));
	}
	else
	{
		increaseOrCreate(sTriangle_save, s->getId(), this->getNrTriangles(s));
		increment += this->getNrTriangle_extended(graph->getDiGraph()->vertexAt(s->getId()), graph->getDiGraph()->vertexAt(tail->getId()));
	}
	return increment;
}

/**
 * @brief Gives the change of the s-count regarding the subgraph three-path for the vertex s when an edge that is given by two vertices is inserted/deleted.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @param s vertex for which the change in s-count should be updated
 * @return int 
 */
int SubgraphCounts::sTPathCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s)
{
	int increment{0};
	if (head == s || tail == s)
	{

		increment += getNrTPaths_extended(head, tail);
	}
	else
	{

		bool Esu = StrucCount.EpsTab.arcExists(s, head);
		bool Esv = StrucCount.EpsTab.arcExists(s, tail);

		if (Esu && !Esv)
		{

			increment += graph->getDiGraph()->getDegree(s, 0) - 1 + graph->getDiGraph()->getDegree(tail, 0) - 1;
		}
		if (!Esu && Esv)
		{

			increment += graph->getDiGraph()->getDegree(s, 0) - 1 + graph->getDiGraph()->getDegree(head, 0) - 1;
		}
		if (Esu && Esv)
		{

			increment += 2 * (graph->getDiGraph()->getDegree(s, 0) - 2) + graph->getDiGraph()->getDegree(tail, 0) - 2 + graph->getDiGraph()->getDegree(head, 0) - 2;
		}

		int p = StrucCount.getNruLv2(s, head);

		if (Esv && StrucCount.EpsTab.is_low_deg2(tail))
			p -= 1;

		increment += p;

		p = StrucCount.getNruLv2(s, tail);

		if (Esu && StrucCount.EpsTab.is_low_deg2(head))
			p -= 1;

		for (auto h = StrucCount.EpsTab.getEpsbegin2(); h != StrucCount.EpsTab.getEpsend2(); h++)
		{
			if (h->second == s)
				continue;
			bool Ehs = StrucCount.EpsTab.arcExists(h->second, s);
			if (h->second != tail)
			{
				if (Ehs && StrucCount.EpsTab.arcExists(h->second, head))
					p++;
			}
			if (h->second != head)
			{
				if (Ehs && StrucCount.EpsTab.arcExists(h->second, tail))
					p++;
			}
		}

		increment += p;
	}

	return increment;
}

/**
 * @brief Gives the change of the s-count regarding the subgraph claw for the vertex s when an edge that is given by two vertices is inserted/deleted.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @param s vertex for which the change in s-count should be updated
 * @param deg_h degree of vertex head
 * @param deg_t degree of vertex tail
 * @return int 
 */
int SubgraphCounts::sClawCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s, int deg_h, int deg_t)
{
	int increment{0};

	if (head == s || tail == s)
	{
		increment += getNrClaws_extended(head, tail);
	}
	else
	{
		if (deg_h >= 1 && StrucCount.EpsTab.arcExists(s, head))
		{

			increment += deg_h - 1;
		}
		if (deg_t >= 1 && StrucCount.EpsTab.arcExists(s, tail))
		{

			increment += deg_t - 1;
		}
	}
	return increment;
}

/**
 * @brief Gives the change of the s-count regarding the subgraph paw for the vertex s when an edge that is given by two vertices is inserted/deleted.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @param s vertex for which the change in s-count should be updated
 * @param tHead Number of triangels the vertex head is part of
 * @param tTial Number of triangels the vertex tail is part of
 * @param tHeadTail Number of triangels the edge head-tail is part of
 * @return int 
 */
int SubgraphCounts::sPawCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s, bool arc_removed, int tHead, int tTail, int tHeadTail)
{
	int increment{0};
	if (head == s || tail == s)
	{
		increment += getNrPaws_extended(head, tail, tHead, tTail, tHeadTail);
	}
	else
	{
		if (StrucCount.EpsTab.arcExists(head, s) && !StrucCount.EpsTab.arcExists(tail, s))
		{
			int p1{0}, p2{0};
			if (arc_removed)
			{
				p1 = sPawArcWithEdge(head, s, head, tail, false);
				p2 = sPawArcWithEdge(head, s, head, tail, true);
			}
			else
			{
				p1 = sPawArcWithoutEdge(head, s, nullptr, nullptr);
				p2 = sPawArcWithoutEdge(head, s, head, tail);
			}
			int p = std::abs(p1 - p2);

			increment += p;
		}
		if (StrucCount.EpsTab.arcExists(tail, s) && !StrucCount.EpsTab.arcExists(head, s))
		{
			int p1{0}, p2{0};
			if (arc_removed)
			{
				p1 = sPawArcWithEdge(tail, s, head, tail, false);
				p2 = sPawArcWithEdge(tail, s, head, tail, true);
			}
			else
			{
				p1 = sPawArcWithoutEdge(tail, s, nullptr, nullptr);
				p2 = sPawArcWithoutEdge(tail, s, head, tail);
			}
			int p = std::abs(p1 - p2);

			increment += p;
		}

		if (StrucCount.EpsTab.arcExists(tail, s) && StrucCount.EpsTab.arcExists(head, s))
		{
			int p1_1{0}, p1_2{0}, p2_1{0}, p2_2{0};

			if (arc_removed)
			{
				p1_1 = sPawArcWithEdge(head, s, head, tail, false);
				p1_2 = sPawArcWithEdge(head, s, head, tail, true);
				p2_1 = sPawArcWithEdge(tail, s, head, tail, false);
				p2_2 = sPawArcWithEdge(tail, s, head, tail, true);
			}
			else
			{
				p1_1 = sPawArcWithoutEdge(head, s, nullptr, nullptr);
				p1_2 = sPawArcWithoutEdge(head, s, head, tail);
				p2_1 = sPawArcWithoutEdge(tail, s, nullptr, nullptr);
				p2_2 = sPawArcWithoutEdge(tail, s, head, tail);
			}
			int p1 = std::abs(p1_1 - p1_2);
			int p2 = std::abs(p2_1 - p2_2);

			Algora::GraphArtifact::size_type degs{graph->getDiGraph()->getDegree(s, 0)},
				degh{graph->getDiGraph()->getDegree(head, 0)},
				degt{graph->getDiGraph()->getDegree(tail, 0)};

			int p = p1 + p2;
			if (degs > 2)
				p -= degs - 2;
			if (degt > 2)
				p -= degt - 2;
			if (degh > 2)
				p -= degh - 2;

			increment += p;
		}

		increment += StrucCount.getNrcL(tail, head, s);
		int p = 0;
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
			if (StrucCount.EpsTab.arcExists(tail, h->second) && StrucCount.EpsTab.arcExists(s, h->second) && StrucCount.EpsTab.arcExists(head, h->second))
				p++;
		}

		increment += p;
	}
	return increment;
}

/**
 * @brief Gives the change of the s-count regarding the subgraph four-cycle for the vertex s when an edge that is given by two vertices is inserted/deleted.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @param s vertex for which the change in s-count should be updated
 * @return int 
 */
int SubgraphCounts::sFCycleCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s)
{
	int increment{0};
	if (s == head || s == tail)
	{

		increment += getNrfCycles_extended(head, tail);
	}

	else
	{
		bool Esh = StrucCount.EpsTab.arcExists(s, head);
		bool Est = StrucCount.EpsTab.arcExists(s, tail);
		int p1{0}, p2{0};

		if (Esh)
		{
			p1 = StrucCount.getNruLv(s, tail);
		}
		if (Est)
		{
			p2 = StrucCount.getNruLv(s, head);
		}

		if (Esh || Est)
			for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
			{
				if (!StrucCount.EpsTab.arcExists(s, h->second))
					continue;
				if (Esh && (StrucCount.EpsTab.arcExists(tail, h->second) || h->second == head))
					p1++;
				if (Est && (StrucCount.EpsTab.arcExists(head, h->second) || h->second == tail))
					p2++;
			}

		if (p1 > 0)
			p1--;
		if (p2 > 0)
			p2--;

		increment += p1 + p2;
	}

	return increment;
}

/**
 * @brief Gives the change of the s-count regarding the subgraph diamond for the vertex s when an edge that is given by two vertices is inserted/deleted.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @param s vertex for which the change in s-count should be updated
 * @return int 
 */
int SubgraphCounts::sDiamondCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s)
{
	int increment{0};

	if (s == head || s == tail)
	{

		increment += getNrDiamonds_extended(head, tail);
	}
	else
	{
		if (StrucCount.EpsTab.arcExists(head, s) && StrucCount.EpsTab.arcExists(tail, s))
		{

			increment += this->getNrTriangle_extended_with_edge(head, tail, head, tail) - 1;

			increment += this->getNrTriangle_extended_with_edge(s, tail, head, tail) - 1;

			increment += this->getNrTriangle_extended_with_edge(s, head, head, tail) - 1;
		}

		if ((StrucCount.EpsTab.arcExists(s, head) && !StrucCount.EpsTab.arcExists(s, tail)) || (StrucCount.EpsTab.arcExists(s, tail) && !StrucCount.EpsTab.arcExists(s, head)))
		{

			increment += StrucCount.getNrcL(s, head, tail);
		}
		else if (StrucCount.EpsTab.arcExists(s, head) && StrucCount.EpsTab.arcExists(s, tail))
		{

			increment += 2 * StrucCount.getNrcL(s, head, tail);
		}

		int p{0};
		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
			if (StrucCount.EpsTab.arcExists(h->second, s) && StrucCount.EpsTab.arcExists(h->second, head) && StrucCount.EpsTab.arcExists(h->second, tail))
			{
				if (StrucCount.EpsTab.arcExists(s, head))
					p++;
				if (StrucCount.EpsTab.arcExists(s, tail))
					p++;
			}
		}

		increment += p;
	}
	return increment;
}

/**
 * @brief Gives the change of the s-count regarding the subgraph four-clique for the vertex s when an edge that is given by two vertices is inserted/deleted.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @param s vertex for which the change in s-count should be updated
 * @return int 
 */
int SubgraphCounts::sFCliqueCount(const Algora::Vertex *head, const Algora::Vertex *tail, const Algora::Vertex *s)
{
	int increment{0};
	if (s == head || s == tail)
	{

		increment += getNrFCliques_extended(head, tail);
	}

	else
	{
        if (StrucCount.EpsTab.arcExists(head,s) && StrucCount.EpsTab.arcExists(tail,s))
        {
            increment += StrucCount.getNrcL2(head,tail,s);
            for (auto h = StrucCount.EpsTab.getEpsbegin2(); h != StrucCount.EpsTab.getEpsend2(); h++)
            {
                if (h->second == head || h->second == tail)
                    continue;
                if (StrucCount.EpsTab.arcExists(h->second,s) && StrucCount.EpsTab.arcExists(h->second,head) && StrucCount.EpsTab.arcExists(h->second,tail))
                    increment++;
            }
        }
	}
	return increment;
}
