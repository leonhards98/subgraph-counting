#pragma once

#include "SubgraphAuxCounts.h"
#include "graph.dyn/dynamicdigraph.h"
#include "graph.incidencelist/incidencelistvertex.h"
#include "graph/arc.h"
#include "graph/digraph.h"
#include "graph/graph_functional.h"
#include "graph/graphartifact.h"
#include "graph/vertex.h"
#include <boost/container_hash/extensions.hpp>
#include <boost/container_hash/hash_fwd.hpp>
#include <boost/unordered/unordered_map.hpp>
#include <chrono>
#include <functional>
#include <stdexcept>
#include <cmath>
#include <vector>
#include <string>
#include "observable.h"
#include <iostream>
#include "flat_hash_map/bytell_hash_map.hpp"
#include "property/fastpropertymap.h"

namespace std
{
	/**
	 * @brief Hahs function for std::pair<int,int> by concatenating the two 32-bit integers
 	* and the using the staard hash function on the new 64-bit integer.
 	* 
 	*/
	template <>
	struct hash<std::pair<int, int>>
	{
		auto operator()(const std::pair<int, int> &key) const -> size_t
		{
			boost::hash<size_t> hasher;
			return hasher((size_t(uint32_t(key.first)) << 32) | size_t(uint32_t(key.second)));

			// return (size_t(uint32_t(key.first)) << 32) | size_t(uint32_t(key.second));
		}
	};
}
/**
 * @brief Manages two epsilon partitions with the changes in the given graph.
 * Provides observers for the changes in the graph.
 * Calls the function "on_reompute_func" when a recomputation is needed
 * 
 */
class EpsilonTab : public SubgraphAuxCounts
{
	double epsilon1{-1};
	double epsilon2{-1};
	int nr_edges;
	int M;
	boost::unordered_map<int, const Algora::Vertex *> HighDeg1;
	boost::unordered_map<int, const Algora::Vertex *> HighDeg2;
	double theta1;
	double theta2;
	bool fixed{false};
	std::chrono::duration<double> currentDynTime{0};

	std::function<void(bool arcAdd)> on_reompute_func = nullptr;

	Algora::Observable<Algora::Vertex *> observableVertexUp1;
	Algora::Observable<Algora::Vertex *> observableVertexUp2;
	Algora::Observable<Algora::Vertex *> observableVertexDown1;
	Algora::Observable<Algora::Vertex *> observableVertexDown2;
	Algora::Observable<Algora::Arc *> observableArcAdd;
	Algora::Observable<Algora::Arc *> observableArcRemove;

	ska::bytell_hash_map<std::pair<int, int>, bool> adjacencyMap;

	bool recompute(Algora::Arc *a, bool arcAdd);
	bool check_recompute(Algora::Arc *a, bool arcAdd);

public:
	boost::unordered_map<Algora::Arc *, int> insertedArcs;

	bool setEpsilon(const double epsilon, const double epsilon2);
	bool prepare();
	void cleanup();
	void print_table();
	void onVertexToHigh1(void *id, const Algora::VertexMapping &vvFun);
	void onVertexToLow1(void *id, const Algora::VertexMapping &vvFun);
	void onVertexToHigh2(void *id, const Algora::VertexMapping &vvFun);
	void onVertexToLow2(void *id, const Algora::VertexMapping &vvFun);
	void onArcAdd(void *id, const Algora::ArcMapping &vvFun);
	void onArcRemove(void *id, const Algora::ArcMapping &vvFun);
	void removeOnVertexToLow1(void *id);
	void removeOnVertexToHigh1(void *id);
	void removeOnVertexToLow2(void *id);
	void removeOnVertexToHigh2(void *id);
	void removeOnArcAdd(void *id);
	void removeOnArcRemove(void *id);
	void EpsTabOnArcRemove(Algora::Arc *a);
	void EpsTabOnArcAdd(Algora::Arc *a);
	void EpsTabOnVertexRemove(Algora::Vertex *v);
	/**
	 * @brief Get the time that was required for the last operations
	 * 
	 * @return std::chrono::duration<double> 
	 */
	std::chrono::duration<double> getCurrentDynTime()
	{
		return currentDynTime;
	}
	/**
	 * @brief Fixes the tables so no vertices are moved between partitions.
	 * Also, no observers are notified when a operation in the graph takes place.
	 * 
	 */
	void fixTable()
	{
		fixed = true;
	}
	/**
	 * @brief Unfixes the tables so vertices are again moved between partitions and observers are notified
	 * 
	 */
	void unfixTable()
	{
		fixed = false;
	}
	/**
	 * @brief Set the On Recompute object
	 * 
	 * @param on_recompute_func Function to be called whenever a recomputation is required. Is called after the partitions have been updated
	 */
	void setOnRecompute(std::function<void(bool arcAdd)> on_recompute_func)
	{
		this->on_reompute_func = on_recompute_func;
	}
	/**
	 * @brief removes the function
	 * 
	 */
	void removeOnRecompute()
	{
		this->on_reompute_func = nullptr;
	}
	/**
	 * @brief checks if the given vertex is high or low degree according to partition 1
	 * 
	 * @param id id of the vertex to check
	 */
	bool is_high_deg(const Algora::Vertex::id_type id)
	{
		return is_high_deg1(id);
	}
	/**
	 * @brief checks if the given vertex is high or low degree according to partition 1
	 * 
	 * @param id id of the vertex to check
	 */
	bool is_high_deg1(const Algora::Vertex::id_type id)
	{

		if (!hasGraph())
			throw std::runtime_error("Need to set Graph first");
		return (HighDeg1.find(id) != HighDeg1.end());
	}
	/**
	 * @brief checks if the given vertex is high or low degree according to partition 2
	 * 
	 * @param ud id of the vertex to check
	 */
	bool is_high_deg2(const Algora::Vertex::id_type id)
	{

		if (!hasGraph())
			throw std::runtime_error("Need to set Graph first");
		return (HighDeg2.find(id) != HighDeg2.end());
	}
	/**
	 * @brief checks if the given vertex is high or low degree according to partition 1
	 * 
	 * @param v vertex to check
	 */
	bool is_high_deg(const Algora::Vertex *v)
	{
		return is_high_deg1(v);
	}
	/**
	 * @brief checks if the given vertex is high or low degree according to partition 1
	 * 
	 * @param v vertex to check
	 */
	bool is_high_deg1(const Algora::Vertex *v)
	{
		if (!hasGraph())
			throw std::runtime_error("Need to set Graph first");
		return (HighDeg1.find(v->getId()) != HighDeg1.end());
	}
	/**
	 * @brief checks if the given vertex is high or low degree according to partition 2
	 * 
	 * @param v vertex to check
	 */
	bool is_high_deg2(const Algora::Vertex *v)
	{
		if (!hasGraph())
			throw std::runtime_error("Need to set Graph first");
		return (HighDeg2.find(v->getId()) != HighDeg2.end());
	}

	/**
	 * @brief checks if the given vertex is high or low degree according to partition 1
	 * 
	 * @param id id of vertex to check
	 */
	bool is_low_deg(const Algora::Vertex::id_type id)
	{
		return is_low_deg1(id);
	}
	/**
	 * @brief checks if the given vertex is high or low degree according to partition 1
	 * 
	 * @param id id of vertex to check
	 */
	bool is_low_deg1(const Algora::Vertex::id_type id)
	{
		return !is_high_deg1(id);
	}
	/**
	 * @brief checks if the given vertex is high or low degree according to partition 2
	 * 
	 * @param id id of vertex to check
	 */
	bool is_low_deg2(const Algora::Vertex::id_type id)
	{
		return !is_high_deg2(id);
	}

	/**
	 * @brief checks if the given vertex is high or low degree according to partition 1
	 * 
	 * @param v vertex to check
	 */
	bool is_low_deg(const Algora::Vertex *v)
	{
		return is_low_deg1(v);
	}
	/**
	 * @brief checks if the given vertex is high or low degree according to partition 1
	 * 
	 * @param v vertex to check
	 */
	bool is_low_deg1(const Algora::Vertex *v)
	{
		return !is_high_deg1(v);
	}
	/**
	 * @brief checks if the given vertex is high or low degree according to partition 2
	 * 
	 * @param v vertex to check
	 */
	bool is_low_deg2(const Algora::Vertex *v)
	{
		return !is_high_deg2(v);
	}
	/**
	 * @brief Returns the iterator to the beginning of the vector that contains all high degree vertices of partition 1
	 * 
	 * @return boost::unordered_map<int, const Algora::Vertex *>::const_iterator 
	 */
	boost::unordered_map<int, const Algora::Vertex *>::const_iterator getEpsbegin() const
	{
		return getEpsbegin1();
	}
	/**
	 * @brief Returns the iterator to the beginning of the vector that contains all high degree vertices of partition 1
	 * 
	 * @return boost::unordered_map<int, const Algora::Vertex *>::const_iterator 
	 */
	boost::unordered_map<int, const Algora::Vertex *>::const_iterator getEpsbegin1() const
	{
		return HighDeg1.begin();
	}
	/**
	 * @brief Returns the iterator to the beginning of the vector that contains all high degree vertices of partition 2
	 * 
	 * @return boost::unordered_map<int, const Algora::Vertex *>::const_iterator 
	 */
	boost::unordered_map<int, const Algora::Vertex *>::const_iterator getEpsbegin2() const
	{
		return HighDeg2.begin();
	}
	/**
	 * @brief Returns the iterator to the end of the vector that contains all high degree vertices of partition 1
	 * 
	 * @return boost::unordered_map<int, const Algora::Vertex *>::const_iterator 
	 */
	boost::unordered_map<int, const Algora::Vertex *>::const_iterator getEpsend() const
	{
		return getEpsend1();
	}
	/**
	 * @brief Returns the iterator to the end of the vector that contains all high degree vertices of partition 1
	 * 
	 * @return boost::unordered_map<int, const Algora::Vertex *>::const_iterator 
	 */
	boost::unordered_map<int, const Algora::Vertex *>::const_iterator getEpsend1() const
	{
		return HighDeg1.end();
	}
	/**
	 * @brief Returns the iterator to the end of the vector that contains all high degree vertices of partition 2
	 * 
	 * @return boost::unordered_map<int, const Algora::Vertex *>::const_iterator 
	 */
	boost::unordered_map<int, const Algora::Vertex *>::const_iterator getEpsend2() const
	{
		return HighDeg2.end();
	}

	/**
	 * @brief Checks if an arc between two vertices exists using the adjacency map maintained by this class
	 * If the table is fixed, uses the algora framework
	 * 
	 * @param v1 first vertex
	 * @param v2 second vertex
	 */
	inline bool arcExists(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		if (fixed)
		{

			if (graph->getDiGraph()->findArc(v1, v2) != nullptr || graph->getDiGraph()->findArc(v2, v1) != nullptr)
				return true;
			return false;
		}

		std::pair<int, int> key;
		if (v1->getId() < v2->getId())
		{
			key = std::pair<int, int>({v1->getId(), v2->getId()});
		}

		else
		{
			key = std::pair<int, int>({v2->getId(), v1->getId()});
		}

		auto it = adjacencyMap.find(key);

		if (it == adjacencyMap.end() || it->second == false)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	 * @brief Get the number of high degree vertices in partition 1
	 * 
	 * @return int 
	 */
	int get_eps_size1()
	{
		return HighDeg1.size();
	}
	/**
	 * @brief Get the number of high degree vertices in partition 2
	 * 
	 * @return int 
	 */
	int get_eps_size2()
	{
		return HighDeg2.size();
	}
};
