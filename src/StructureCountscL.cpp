#include "StructureCounts.h"
#include "graph/graph_functional.h"

/**
 * @brief updates the cL-auxiliary count by calculating the number the count should change by when inserting/removing the given arc. 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the arc is added or deleted. 
 * 
 * @param a arc that is added/deleted
 * @param operation function that either increments or decrements the a given map. This should decrement if the arc is removed and increment if 
 * the arc in inserted
 * @param map the hash map that contains the the key-value pairs of all vertices that are part of this structure as an anchor vertex and 
 * the count of that structure
 * @param EpsNr what number of epsilon table should be used
 */
void StructureCounts::cLArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> &, int first, int second, int third, int), ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> &map, int EpsNr)
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	bool done {false};
	bool head_low, tail_low;

	if (EpsNr == 1)
	{
		head_low = EpsTab.is_low_deg1(head);
		tail_low = EpsTab.is_low_deg1(tail);
	}
	else
	{
		head_low = EpsTab.is_low_deg2(head);
		tail_low = EpsTab.is_low_deg2(tail);
	}

	
	if (head_low)
	{

		graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(head);
			if (x == tail)
				return;
			done = false;
			graph->getDiGraph()->mapIncidentArcsUntil(head,[&](const Algora::Arc *a2)
			{
				auto y = a2->getOther(head);
				if (y == tail)
					return;
				operation(map,x->getId(),y->getId(),tail->getId(),1);

			},[&done,&a1](const Algora::Arc* a_now) 
			{
				if (done == false)
					done = (a1 == a_now);
				return done;
			});
		});
	}

	if (tail_low)
	{

		graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
		{
			auto x = a1->getOther(tail);
			if (x == head)
				return;
			done = false;
			graph->getDiGraph()->mapIncidentArcsUntil(tail,[&](const Algora::Arc *a2)
			{
				auto y = a2->getOther(tail);
				if (y == head)
					return;
				operation(map,x->getId(),y->getId(),head->getId(),1);

			},[&done,&a1](const Algora::Arc* a_now) 
			{
				if (done == false)
					done = (a1 == a_now);
				return done;
			});
		});


	}


}

/**
 * @brief updates the cL-auxiliary count by calculating the number the count should change by when the given vertex changes from one epsilon partition to the other 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the vertex changes from low to high or the other way around
 * 
 * @param v vertex that changes partition
 * @param operation function that either increments or decrements the a given map. This should decrement if the vertex becomes high degree and increment if 
 * the vertex becomes low degree
 * @param map the hash map that contains the the key-value pairs of all vertices that are part of this structure as an anchor vertex and 
 * the count of that structure
 */
void StructureCounts::cLVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> &, int first, int second, int third, int), ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> &map)
{
	bool done {false};
	bool done2 {false};


	graph->getDiGraph()->mapIncidentArcs(v,[&](const Algora::Arc *a1)
	{
		auto x = a1->getOther(v);
		
		done = false;
		graph->getDiGraph()->mapIncidentArcsUntil(v,[&](const Algora::Arc *a2)
		{

			auto y = a2->getOther(v);
			done2 = false;
			graph->getDiGraph()->mapIncidentArcsUntil(v,[&](const Algora::Arc *a3)
			{
				auto z = a3->getOther(v);
				operation(map,x->getId(),y->getId(),z->getId(),1);

			},[&done2,&a2](const Algora::Arc* a_now) 
			{
				if (done2 == false)
					done2 = (a2 == a_now);
				return done2;
			});
		},[&done,&a1](const Algora::Arc* a_now) 
		{
			if (done == false)
				done = (a1 == a_now);
			return done;
		});
	});

	

}
