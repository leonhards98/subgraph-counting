/**
 * This file contains all functions that count structures that contain a certain vertex or edge without any temporal knowledge.
 * Those are auxiliary functions for the s-counts.
 * They are also used to count the number of structures thazt contain an edge after it was inserted/removed in order to update the total counts.
 * 
 */
#include "SubgraphCounts.h"
#include "graph.incidencelist/incidencelistvertex.h"
#include <cmath>
#include <boost/math/special_functions/binomial.hpp>
#include <boost/math/special_functions/math_fwd.hpp>
#include <iostream>

/**
 * @brief Count the number of claws that contain the the edge that is given by the two vertices.
 * The vertices can be given in any order.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @return int 
 */
int SubgraphCounts::getNrClaws_extended(const Algora::Vertex *head, const Algora::Vertex *tail)
{
	int deg_h = graph->getDiGraph()->getDegree(head, 0) - 1;
	int deg_t = graph->getDiGraph()->getDegree(tail, 0) - 1;

	int ret{0};
	if (deg_t >= 2)
	{
		ret += 0.5 * deg_t * (deg_t - 1);
	}

	if (deg_h >= 2)
	{
		ret += 0.5 * deg_h * (deg_h - 1);
	}
	return ret;
}

/**
 * @brief Count the number of three-paths that contain the the edge that is given by the two vertices.
 * The vertices can be given in any order.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @return int 
 */
int SubgraphCounts::getNrTPaths_extended(const Algora::Vertex *head, const Algora::Vertex *tail)
{
	int ret{0};
	ret += (graph->getDiGraph()->getDegree(head, 0) - 1) * (graph->getDiGraph()->getDegree(tail, 0) - 1) - getNrTriangle_extended(graph->getDiGraph()->vertexAt(head->getId()), graph->getDiGraph()->vertexAt(tail->getId()));

	int p1 = StrucCount.getNrvLV(tail) - StrucCount.getNruLv2(head, tail);
	int p2 = StrucCount.getNrvLV(head) - StrucCount.getNruLv2(head, tail);

	if (StrucCount.EpsTab.is_low_deg2(head))
		p1 -= graph->getDiGraph()->getDegree(head, 0) - 1;

	if (StrucCount.EpsTab.is_low_deg2(tail))
		p2 -= graph->getDiGraph()->getDegree(tail, 0) - 1;

	if (p1 > 0)
		ret += p1;
	if (p2 > 0)
		ret += p2;

	for (auto h = StrucCount.EpsTab.getEpsbegin2(); h != StrucCount.EpsTab.getEpsend2(); h++)
	{
		if (h->second == head || h->second == tail)
			continue;
		bool Ehu = StrucCount.EpsTab.arcExists(h->second, head);
		bool Ehv = StrucCount.EpsTab.arcExists(h->second, tail);

		if (Ehu)
		{
			if (Ehv)
			{

				int tmp = graph->getDiGraph()->getDegree(h->second, 0) - 2;
				if (tmp > 0)
					ret += tmp;
			}
			else
			{

				int tmp = graph->getDiGraph()->getDegree(h->second, 0) - 1;
				if (tmp > 0)
					ret += tmp;
			}
		}

		if (Ehv)
		{
			if (Ehu)
			{

				int tmp = graph->getDiGraph()->getDegree(h->second, 0) - 2;
				if (tmp > 0)
					ret += tmp;
			}
			else
			{

				int tmp = graph->getDiGraph()->getDegree(h->second, 0) - 1;
				if (tmp > 0)
					ret += tmp;
			}
		}
	}
	return ret;
}

/**
 * @brief Count the number of triangles that contain the the edge that is given by the two vertices.
 * The vertices can be given in any order.
 * 
 * @param u vertex one of the edge
 * @param v vertex two of the edge
 * @return int 
 */
int SubgraphCounts::getNrTriangle_extended(Algora::IncidenceListVertex *u, Algora::IncidenceListVertex *v)
{
	int triangle{0};

	for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
	{
		if (v == h->second || u == h->second)
			continue;
		if (StrucCount.EpsTab.arcExists(h->second, v) && StrucCount.EpsTab.arcExists(h->second, u))
			triangle++;
	}

	return triangle + StrucCount.getNruLv(u, v);
}

/**
 * @brief Count the number of triangles that contain the the edge that is given by the two vertices.
 * The vertices can be given in any order.
 * Additionally, the function behaves as if there were an edge between the vertices e1 and e2
 * 
 * @param u vertex one of the edge
 * @param v vertex two of the edge
 * @param e1 vertex one of the edge that this function pretends exists
 * @param e2 vertex two of the edge that this function pretends exists
 * @return int 
 */
int SubgraphCounts::getNrTriangle_extended_with_edge(const Algora::Vertex *u, const Algora::Vertex *v, const Algora::Vertex *e1, const Algora::Vertex *e2)
{
	int triangle{0};

	for (auto h = StrucCount.EpsTab.getEpsbegin2(); h != StrucCount.EpsTab.getEpsend2(); h++)
	{
		if ((StrucCount.EpsTab.arcExists(h->second, u) || (e1 == h->second && e2 == u) || (e2 == h->second && e1 == u)) && (StrucCount.EpsTab.arcExists(h->second, v) || (e1 == h->second && e2 == v) || (e2 == h->second && e1 == v)))
			triangle++;
	}

	return triangle + StrucCount.getNruLv2(u, v);
}

/**
 * @brief Count the number of triangles that contain the the vertex that is given.
 * Additionally, the function behaves as if there were an edge between the vertices e1 and e2 that was removed in this step.
 * Assumes the auxiliary counts are not yet updated.
 * 
 * @param v vertex to consider
 * @param e1 vertex one of the edge that this function pretends exists
 * @param e2 vertex two of the edge that this function pretends exists
 * @return int 
 */
int SubgraphCounts::getNrTriangle_extended_with_edge(const Algora::Vertex *v, const Algora::Vertex *e1, const Algora::Vertex *e2)
{
	/* Acts as if the given edge is removed but all Aux-Counts affiliated with the edge are not updated yet */
	if (StrucCount.EpsTab.is_high_deg(v))
	{
		return StrucCount.getNrt(v);
	}
	else
	{
		bool done{false};
		int ret{0};
		graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a)
		{
			auto x = a->getOther(v);

			if ((e1 == v && StrucCount.EpsTab.arcExists(e2, x)) || (e2 == v && StrucCount.EpsTab.arcExists(e1, x)))
				ret++;
			done = false;
			graph->getDiGraph()->mapIncidentArcsUntil(
				v, [&](const Algora::Arc *a2)
				{
					auto y = a2->getOther(v);

					if (StrucCount.EpsTab.arcExists(x, y))
						ret++;
				},
				[&done, &a](const Algora::Arc *a_now)
				{
					if (done == false)
						done = (a == a_now);
					return done;
				});
		});

		return ret;
	}
}

/**
 * @brief Count the number of triangles that contain the the vertex that is given.
 * 
 * @param v vertex to consider

 * @return int 
 */
int SubgraphCounts::getNrTriangle_extended(const Algora::Vertex *v)
{
	if (StrucCount.EpsTab.is_high_deg(v))
	{
		return StrucCount.getNrt(v);
	}
	else
	{
		bool done{false};
		int ret{0};
		graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a)
		{
			auto x = a->getOther(v);

			done = false;
			graph->getDiGraph()->mapIncidentArcsUntil(
				v, [&](const Algora::Arc *a2)
				{
					auto y = a2->getOther(v);

					if (StrucCount.EpsTab.arcExists(x, y))
						ret++;
				},
				[&done, &a](const Algora::Arc *a_now)
				{
					if (done == false)
						done = (a == a_now);
					return done;
				});
		});

		return ret;
	}
}

/**
 * @brief Count the number of claws that contain the  edge that is given by the two vertices.
 * The vertices can be given in any order.
 * Uses auxiliary counts.
 * 
 * @param u vertex one of the edge
 * @param v vertex two of the edge
 * @return int 
 */
int SubgraphCounts::getNrClws_extended(const Algora::Vertex *u, const Algora::Vertex *v)
{
	int p = StrucCount.getNrcLV(u, v);

	for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
	{
		if (StrucCount.EpsTab.arcExists(u, h->second) && StrucCount.EpsTab.arcExists(v, h->second))
			p += graph->getDiGraph()->getDegree(h->second, 0) - 2;
	}

	return p;
}

/**
 * @brief Counts the number of paws that countain the edge that is given by the two vertices. 
 * Acts as if the edge e1-e2 would not exist. e1-e2 must exist in reality.
 * Both edges mut not be identical
 * The vertices of the edges can be given in any order
 * 
 * @param u vertex one of the edge that should be contained
 * @param v vertex two of the edge that should be contained
 * @param e1 vertex one of the edge that should not exist
 * @param e2 vertex two of the edge that should not exist
 * @return int 
 */
int SubgraphCounts::sPawArcWithoutEdge(const Algora::Vertex *u, const Algora::Vertex *v, const Algora::Vertex *e1, const Algora::Vertex *e2)
{
	int tuv = this->getNrTriangle_extended(graph->getDiGraph()->vertexAt(u->getId()), graph->getDiGraph()->vertexAt(v->getId()));

	bool Eu1, Eu2, Ev1, Ev2;

	if (e1)
	{
		Eu1 = StrucCount.EpsTab.arcExists(u, e1);
		Eu2 = StrucCount.EpsTab.arcExists(u, e2);
		Ev1 = StrucCount.EpsTab.arcExists(v, e1);
		Ev2 = StrucCount.EpsTab.arcExists(v, e2);
		if (u == e1)
			if (Ev2)
				tuv--;
		if (u == e2)
			if (Ev1)
				tuv--;
		if (v == e1)
			if (Eu2)
				tuv--;
		if (v == e2)
			if (Eu1)
				tuv--;
	}
	if (tuv < 0)
		tuv = 0;

	int deg_u = graph->getDiGraph()->getDegree(u, 1) - 1;

	if (e1)
	{
		if (u == e1 || u == e2)
			deg_u--;
	}
	if (deg_u < 0)
		deg_u = 0;

	int deg_v = graph->getDiGraph()->getDegree(v, 1) - 1;

	if (e1)
	{
		if (v == e1 || v == e2)
			deg_v--;
	}
	if (deg_v < 0)
		deg_v = 0;

	int tu = getNrTriangles(u);

	if (e1)
	{
		if (Eu1 && Eu2)
			tu--;


		if (u == e1 || u == e2)
		{
			tu -= this->getNrTriangle_extended(graph->getDiGraph()->vertexAt(e1->getId()),graph->getDiGraph()->vertexAt(e2->getId()));
		}

	}

	if (tu < 0)
		tu = 0;

	int tv = getNrTriangles(v);

	if (e1)
	{
		if (Ev1 && Ev2)
			tv--;

		if (v == e1 || v == e2)
		{
			tv -= this->getNrTriangle_extended(graph->getDiGraph()->vertexAt(e1->getId()),graph->getDiGraph()->vertexAt(e2->getId()));
		}

	}

	if (tv < 0)
		tv = 0;

	int ret = 0;
	ret += tu + tv - 2 * tuv;

	int p = 0;
	if (deg_u - 1 > 0)
		p += deg_u - 1;
	if (deg_v - 1 > 0)
		p += deg_v - 1;

	p *= tuv;

	ret += p;

	p = StrucCount.getNrcLV(u, v);

	if (e1)
	{
		if (Ev1 && Eu1 && StrucCount.EpsTab.is_low_deg(e1) && v != e2 && u != e2)
			p--;

		if (u == e1)
		{
			if (Ev2 && StrucCount.EpsTab.is_low_deg(e2))
				p -= graph->getDiGraph()->getDegree(e2, 1) - 2;
		}
		if (u == e2)
		{
			if (Ev1 && StrucCount.EpsTab.is_low_deg(e1))
				p -= graph->getDiGraph()->getDegree(e1, 1) - 2;
		}
		if (Ev2 && Eu2 && StrucCount.EpsTab.is_low_deg(e2) && u != e1 && v != e1)
			p--;
		if (v == e1)
		{
			if (Eu2 && StrucCount.EpsTab.is_low_deg(e2))
				p -= graph->getDiGraph()->getDegree(e2, 1) - 2;
		}
		if (v == e2)
		{
			if (Eu1 && StrucCount.EpsTab.is_low_deg(e2))
				p -= graph->getDiGraph()->getDegree(e1, 1) - 2;
		}
	}

	if (p < 0)
		p = 0;

	ret += p;

	p = 0;
	for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
	{
		int mod = 0;
		if (h->second == e1)
		{
			if (u == e2 || v == e2)
				continue;
			else
				mod += 1;
		}
		if (h->second == e2)
		{
			if (u == e1 || v == e1)
				continue;
			else
				mod += 1;
		}

		if (StrucCount.EpsTab.arcExists(u, h->second) && StrucCount.EpsTab.arcExists(v, h->second))
			if (graph->getDiGraph()->getDegree(h->second, 0) - 2 > 0)
			{
				int tmp = graph->getDiGraph()->getDegree(h->second, 0) - 2 - mod;
				if (tmp > 0)
					p += tmp;
			}
	}
	ret += p;

	return ret;
}

/**
 * @brief Counts the number of paws that countain the edge that is given by the two vertices. 
 * Acts as if the edge e1-e2 would exist. e1-e2 must not exist in reality.
 * Both edges mut not be identical
 * If compensate_uLv is true, the function will act as if the edge e1-e2 was removed, but the count for uLv not yet updated.
 * The vertices of the edges can be given in any order
 * 
 * @param u vertex one of the edge that should be contained
 * @param v vertex two of the edge that should be contained
 * @param e1 vertex one of the edge that should not exist
 * @param e2 vertex two of the edge that should not exist
 * @param compensate_uLv descibes if the auxiliary count uLv was already updated that the edge e1-e2 was removed
 * @return int 
 */
int SubgraphCounts::sPawArcWithEdge(const Algora::Vertex *u, const Algora::Vertex *v, const Algora::Vertex *e1, const Algora::Vertex *e2, bool compensate_uLv)
{

	int tuv = this->getNrTriangle_extended(graph->getDiGraph()->vertexAt(u->getId()), graph->getDiGraph()->vertexAt(v->getId()));

	bool Eu1, Eu2, Ev1, Ev2;

	if (e1)
	{
		Eu1 = StrucCount.EpsTab.arcExists(u, e1);
		Eu2 = StrucCount.EpsTab.arcExists(u, e2);
		Ev1 = StrucCount.EpsTab.arcExists(v, e1);
		Ev2 = StrucCount.EpsTab.arcExists(v, e2);
	}

	if (e1 && !compensate_uLv)
	{
		if (u == e1)
			if (Ev2)
				tuv++;
		if (u == e2)
			if (Ev1)
				tuv++;
		if (v == e1)
			if (Eu2)
				tuv++;
		if (v == e2)
			if (Eu1)
				tuv++;
	}
	if (e1)
	{
		if (e1 == u && Ev2 && StrucCount.EpsTab.is_low_deg(e2))
			tuv--;
		else if (e1 == u && Eu2 && StrucCount.EpsTab.is_low_deg(e2))
			tuv--;
		else if (e2 == u && Ev1 && StrucCount.EpsTab.is_low_deg(e1))
			tuv--;
		else if (e2 == u && Eu1 && StrucCount.EpsTab.is_low_deg(e1))
			tuv--;
	}
	if (tuv < 0)
		tuv = 0;

	int deg_u =  graph->getDiGraph()->getDegree(u,1)-2;
	int deg_v =  graph->getDiGraph()->getDegree(v,1)-2;
	int deg_e1 = graph->getDiGraph()->getDegree(e1,1)-1;
	int deg_e2 = graph->getDiGraph()->getDegree(e2,1)-1;

    
    if (e1 == u)
    {
        deg_u = deg_e1;
    }
    else if (e2 == u)
    {
        deg_u = deg_e2;
    }
    if (e1 == v)
    {
        deg_v = deg_e1;
    }
    else if (e2 == v)
    {
        deg_v = deg_e2;
    }

	if (e1 && !compensate_uLv)
	{
		if (u == e1 || u == e2)
			deg_u++;
	}
	if (deg_u < 0)
		deg_u = 0;

	if (e1 && !compensate_uLv)
	{
		if (v == e1 || v == e2)
			deg_u++;
	}
	if (deg_v < 0)
		deg_v = 0;

	int tu = getNrTriangles(u);


	if (e1 && !compensate_uLv)
	{		
		if (Eu1 && Eu2)
			tu++;
	
		if (u == e1)
		{

			graph->getDiGraph()->mapIncidentArcs(u, [&](const Algora::Arc *a)
			{
				auto other = a->getOther(u);

				if (other == e2)
					return;
				if (StrucCount.EpsTab.arcExists(other,e2))
					tu++; 
			});
		}
		if (u == e2)
		{

			graph->getDiGraph()->mapIncidentArcs(u, [&](const Algora::Arc *a)
			{
				auto other = a->getOther(u);
				if (other == e1)
					return;
				if (StrucCount.EpsTab.arcExists(other,e1))
					tu++; 
			});
		}
		
		
	}
	

	if (tu < 0)
		tu = 0;

	int tv = getNrTriangles(v);


	if (e1 && !compensate_uLv)
	{
		
		
		if (Ev1 && Ev2)
			tv++;

		if (v == e1)
		{
			graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a)
			{
				auto other = a->getOther(v);
				if (other == e2)
					return;
				if (StrucCount.EpsTab.arcExists(other,e2))
					tv++; 
			});
		}
		if (v == e2)
		{
			graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a)
			{
				auto other = a->getOther(v);
				if (other == e1)
					return;
				if (StrucCount.EpsTab.arcExists(other,e1))
					tv++; 
			});
		}
		
		
		
	}

	if (tv < 0)
		tv = 0;

	int ret = 0;
	ret += tu + tv - 2 * tuv;
	if (ret < 0)
		ret = 0;

	int p = 0;
	if (deg_u - 1 > 0)
		p += deg_u - 1;
	if (deg_v - 1 > 0)
		p += deg_v - 1;

	p *= tuv;

	ret += p;

	p = StrucCount.getNrcLV(u, v);

	if (e1 && !compensate_uLv)
	{
		if (Ev1 && Eu1 && StrucCount.EpsTab.is_low_deg(e1) && v != e2 && u != e2)
			p++;

		if (u == e1)
		{
			if (Ev2 && StrucCount.EpsTab.is_low_deg(e2))
				p += graph->getDiGraph()->getDegree(e2, 1) - 2;
		}
		if (u == e2)
		{
			if (Ev1 && StrucCount.EpsTab.is_low_deg(e1))
				p += graph->getDiGraph()->getDegree(e1, 1) - 2;
		}
		if (Ev2 && Eu2 && StrucCount.EpsTab.is_low_deg(e2) && u != e1 && v != e1)
			p++;
		if (v == e1)
		{
			if (Eu2 && StrucCount.EpsTab.is_low_deg(e2))
				p += graph->getDiGraph()->getDegree(e2, 1) - 2;
		}
		if (v == e2)
		{
			if (Eu1 && StrucCount.EpsTab.is_low_deg(e2))
				p += graph->getDiGraph()->getDegree(e1, 1) - 2;
		}
	}

	if (p < 0)
		p = 0;

	ret += p;

	p = 0;
	for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
	{
		if (h->second == u || h->second == v)
			continue;
		int mod = 0;
		if (h->second == e1 && !compensate_uLv)
		{
			if ((u == e2 && StrucCount.EpsTab.arcExists(h->second, v)) || (v == e2 && StrucCount.EpsTab.arcExists(h->second, u)))
			{
				if (deg_e1 > 1)
					p += deg_e1 - 1;
				continue;
			}
			else
				mod += 1;
		}
		if (h->second == e2 && !compensate_uLv)
		{
			if ((u == e1 && StrucCount.EpsTab.arcExists(h->second, v)) || (u == e2 && StrucCount.EpsTab.arcExists(h->second, u)))
			{
				if (deg_e2 > 1)
					p += deg_e2 - 1;
				continue;
			}
			else
				mod += 1;
		}

		if (StrucCount.EpsTab.arcExists(u, h->second) && StrucCount.EpsTab.arcExists(v, h->second))
			if (graph->getDiGraph()->getDegree(h->second, 0) - 2 + mod > 0)
			{
				int tmp = graph->getDiGraph()->getDegree(h->second, 0) - 2 + mod;
				if (tmp > 0)
					p += tmp;
			}
	}
	ret += p;

	return ret;
}

/**
 * @brief Count the number of paws that contain the edge that is given by the two vertices head and tail
 * 
 * @param u vertex one of the edge that should be contained
 * @param v vertex two of the edge that should be contained
 * @param tHead Number of triangels the vertex head is part of
 * @param tTial Number of triangels the vertex tail is part of
 * @param tHeadTail Number of triangels the edge head-tail is part of
 * @return int 
 */
int SubgraphCounts::getNrPaws_extended(const Algora::Vertex *head, const Algora::Vertex *tail, int tHead, int tTail, int tHeadTail)
{
	int ret{0};

	ret += tHead + tTail - 2 * tHeadTail;

	int p = 0;
	if (graph->getDiGraph()->getDegree(head, 0) - 2 > 0)
		p += graph->getDiGraph()->getDegree(head, 0) - 2;
	if (graph->getDiGraph()->getDegree(tail, 0) - 2 > 0)
		p += graph->getDiGraph()->getDegree(tail, 0) - 2;

	p *= tHeadTail;

	ret += p;

	ret += StrucCount.getNrcLV(head, tail);

	p = 0;
	for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
	{
		if (StrucCount.EpsTab.arcExists(head, h->second) && StrucCount.EpsTab.arcExists(tail, h->second))
			if (graph->getDiGraph()->getDegree(h->second, 0) - 2 > 0)
				p += graph->getDiGraph()->getDegree(h->second, 0) - 2;
	}

	ret += p;

	return ret;
}

/**
 * @brief Count the number of four-cycles that contain the the edge that is given by the two vertices.
 * The vertices can be given in any order.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @return int 
 */
int SubgraphCounts::getNrfCycles_extended(const Algora::Vertex *head, const Algora::Vertex *tail)
{
	bool head_high = StrucCount.EpsTab.is_high_deg(head);
	bool tail_high = StrucCount.EpsTab.is_high_deg(tail);
	int ret{0};
	int p = 0;

	ret += StrucCount.getNruLLv(head, tail);

	for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
	{
		if (h->second != tail && StrucCount.EpsTab.arcExists(head, h->second))
		{
			if (head_high)
				p += StrucCount.getNruLv(tail, h->second);
			else
			{
				int zw = StrucCount.getNruLv(tail, h->second);
				if (zw > 0)
					p += zw - 1;
			}
		}
		if (h->second != head && StrucCount.EpsTab.arcExists(tail, h->second))
		{
			if (tail_high)
				p += StrucCount.getNruLv(head, h->second);
			else
			{
				int zw = StrucCount.getNruLv(head, h->second);
				if (zw > 0)
					p += zw - 1;
			}
		}
	}

	ret += p;

	if (!head_high && !tail_high)
	{
		p = 0;
		graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *a_h)
		{
			auto x = a_h->getOther(head);

			if (x == tail)
				return;

			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a_t)
			{
				auto y = a_t->getOther(tail);

				if (y == head)
					return;
				if (StrucCount.EpsTab.arcExists(x,y) && StrucCount.EpsTab.is_high_deg(x) && StrucCount.EpsTab.is_high_deg(y))
					p++;
			}); 
		});

		ret += p;
	}
	else
	{
		p = 0;

		for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
		{
			if (h->second == head || h->second == tail)
				continue;
			if (head_high)
			{
				if (StrucCount.EpsTab.arcExists(h->second, tail))
				{
					if (!tail_high)
						p += StrucCount.getNruHv(head, h->second);
					else
					{
						int zw = StrucCount.getNruHv(head, h->second);
						if (zw > 0)
							p += zw - 1;
					}
				}
			}
			else if (tail_high)
			{
				if (StrucCount.EpsTab.arcExists(h->second, head))
				{
					if (!head_high)
						p += StrucCount.getNruHv(tail, h->second);
					else
					{
						int zw = StrucCount.getNruHv(tail, h->second);
						if (zw > 0)
							p += zw - 1;
					}
				}
			}
		}

		ret += p;
	}
	return ret;
}

/**
 * @brief Count the number of diamonds that contain the the edge that is given by the two vertices.
 * The vertices can be given in any order.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @return int 
 */
int SubgraphCounts::getNrDiamonds_extended(const Algora::Vertex *head, const Algora::Vertex *tail)
{
	int ret{0};
	bool done{false};

	bool head_high = StrucCount.EpsTab.is_high_deg(head);
	bool tail_high = StrucCount.EpsTab.is_high_deg(tail);

	if (head_high && tail_high)
	{
		double p = StrucCount.getNruLv(head, tail) + StrucCount.getNruHv(head, tail);
		if (p > 1)
		{
			int pot = boost::math::binomial_coefficient<double>(p, 2.);
			ret += pot;
		}
	}
	else
	{
		if (!head_high)
		{
			graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(head);
				if (x == tail || !StrucCount.EpsTab.arcExists(x,tail))
					return;
				done = false;
				graph->getDiGraph()->mapIncidentArcsUntil(head,[&](const Algora::Arc *a2)
				{
					auto y = a2->getOther(head);
					if (y == tail || !StrucCount.EpsTab.arcExists(y,tail))
						return;

					ret++;
				},[&done,&a1](const Algora::Arc* a_now) 
				{
					if (done == false)
						done = (a1 == a_now);
					return done;
				}); 
			});
		}
		else if (!tail_high)
		{
			graph->getDiGraph()->mapIncidentArcs(tail, [&](const Algora::Arc *a1)
			{
				auto x = a1->getOther(tail);
				if (x == head || !StrucCount.EpsTab.arcExists(x,head))
					return;
				done = false;
				graph->getDiGraph()->mapIncidentArcsUntil(tail,[&](const Algora::Arc *a2)
				{
					auto y = a2->getOther(tail);
					if (y == head || !StrucCount.EpsTab.arcExists(y,head))
						return;

					ret++;
				},[&done,&a1](const Algora::Arc* a_now) 
				{
					if (done == false)
						done = (a1 == a_now);
					return done;
				}); 
			});
		}
	}

	ret += StrucCount.getNrpLL(head, tail);

	for (auto h = StrucCount.EpsTab.getEpsbegin(); h != StrucCount.EpsTab.getEpsend(); h++)
	{
		if (h->second == head || h->second == tail)
			continue;

		bool Euh{StrucCount.EpsTab.arcExists(h->second, head)};
		bool Evh{StrucCount.EpsTab.arcExists(h->second, tail)};
		if (Euh)
		{

			ret += StrucCount.getNrcL(head, tail, h->second);

			if (StrucCount.EpsTab.is_low_deg(tail) && Evh && Euh && h->second != head)
				graph->getDiGraph()->mapIncidentArcs(tail, [&](const Algora::Arc *a)
				{
					auto x = a->getOther(tail);
					if (x == head || x == h->second)
						return;

					if (StrucCount.EpsTab.arcExists(h->second,x))
					{
						ret++;
					} 
				});
		}
		if (Evh)
		{

			ret += StrucCount.getNrcL(head, tail, h->second);

			if (StrucCount.EpsTab.is_low_deg(head) && Euh && Evh && h->second != tail)
				graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *a)
				{
					auto x = a->getOther(head);
					if (x == tail || x == h->second)
						return;

					if (StrucCount.EpsTab.arcExists(h->second,x))
					{
						ret++;
					} 
				});
		}

		if (Euh && Evh)
		{
			if (StrucCount.EpsTab.is_high_deg(head))
				ret += StrucCount.getNruLv(head, h->second) + StrucCount.getNruHv(head, h->second) - 1;
			if (StrucCount.EpsTab.is_high_deg(tail))
				ret += StrucCount.getNruLv(tail, h->second) + StrucCount.getNruHv(tail, h->second) - 1;
		}
	}
	return ret;
}

/**
 * @brief Count the number of four-cliques that contain the the edge that is given by the two vertices.
 * The vertices can be given in any order.
 * 
 * @param head vertex one of the edge
 * @param tail vertex two of the edge
 * @return int 
 */
int SubgraphCounts::getNrFCliques_extended(const Algora::Vertex *head, const Algora::Vertex *tail)
{
	bool head_high = StrucCount.EpsTab.is_high_deg2(head);
	bool tail_high = StrucCount.EpsTab.is_high_deg2(tail);
	int ret{0}, c{0};

    if (head_high && tail_high)
    {
        graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc* a)
        {
            auto y = a->getOther(head);
            if (y == tail || !StrucCount.EpsTab.arcExists(y,tail))
                return;

            if (StrucCount.EpsTab.is_high_deg2(y))
                ret += StrucCount.getNrcL2(head,tail,y);
            else
                c += StrucCount.getNrcL2(head,tail,y);
        });
    }
    else
    {
        if (!head_high)
        {
            graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc* a)
            {
                auto y = a->getOther(head);
                if (y == tail || !StrucCount.EpsTab.arcExists(y,tail))
                    return;

                if (StrucCount.EpsTab.is_high_deg2(y))
                    ret += StrucCount.getNrcL2(head,tail,y);
                else
                    c += StrucCount.getNrcL2(head,tail,y);
            });
        }
        else
        {
            graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc* a)
            {
                auto y = a->getOther(tail);
                if (y == tail || !StrucCount.EpsTab.arcExists(y,head))
                    return;

                if (StrucCount.EpsTab.is_high_deg2(y))
                    ret += StrucCount.getNrcL2(head,tail,y);
                else
                    c += StrucCount.getNrcL2(head,tail,y);
            });
        }


    }

    for (auto h = StrucCount.EpsTab.getEpsbegin2(); h != StrucCount.EpsTab.getEpsend2(); h++)
    {
        for (auto h2 = StrucCount.EpsTab.getEpsbegin2(); h2 != StrucCount.EpsTab.getEpsend2(); h2++)
        {
            if (h == h2)
                break;
            if (h->second == head || h->second == tail || h2->second == head || h2->second == tail)
                continue;

            if (StrucCount.EpsTab.arcExists(h->second,h2->second) && StrucCount.EpsTab.arcExists(h->second,head) && StrucCount.EpsTab.arcExists(h->second,tail) && StrucCount.EpsTab.arcExists(h2->second,head) && StrucCount.EpsTab.arcExists(h2->second,tail))
                    ret++;
        }
    }

	return ret +c/2;
}
