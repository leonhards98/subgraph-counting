#include "StructureCounts.h"
#include "graph/vertex.h"
#include <utility>

/**
 * @brief updates the t-auxiliary count by calculating the number the count should change by when inserting/removing the given arc. 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the arc is added or deleted
 * 
 * @param a arc that is added/deleted
 * @param operation function that either increments or decrements the a given map. This should decrement if the arc is removed and increment if 
 * the arc in inserted
 */
void StructureCounts::tArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int))
{

	Algora::Vertex *head = a->getHead();
	Algora::Vertex *tail = a->getTail();
	bool head_is_high{EpsTab.is_high_deg(head)};
	bool tail_is_high{EpsTab.is_high_deg(tail)};
	int increment{0};

	increment = this->getNruLv(head, tail);

	for (auto it = EpsTab.getEpsbegin(); it != EpsTab.getEpsend(); it++)
	{
		if (it->second == head || it->second == tail)
			continue;

		if (EpsTab.arcExists(it->second, head) && EpsTab.arcExists(it->second, tail))
		{
			operation(t, it->first, 1);

			if (head_is_high)
				operation(t, head->getId(), 1);
			if (tail_is_high)
				operation(t, tail->getId(), 1);
		}
	}
	if (head_is_high)
		operation(t, head->getId(), increment);
	if (tail_is_high)
		operation(t, tail->getId(), increment);
}

/**
 * @brief updates the pLL-auxiliary count by calculating the number the count should change by when the given vertex changes from 
 * high degree to low
 * 
 * @param v vertex that changes partition
 */
void StructureCounts::tVertexToLow(const Algora::Vertex *v)
{
	auto it = t.find(v->getId());

	if (it == t.end())
		return;
	t.erase(it);
	t.shrink_to_fit();
}

/**
 * @brief updates the pLL-auxiliary count by calculating the number the count should change by when the given vertex changes from 
 * low degree to high
 * 
 * @param v vertex that changes partition
 */
void StructureCounts::tVertexToHigh(const Algora::Vertex *v)
{
	bool done{false};

	graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a1)
	{
		auto x = a1->getOther(v);
		
		done = false;
		graph->getDiGraph()->mapIncidentArcsUntil(v,[&](const Algora::Arc *a2)
		{			
			auto y = a2->getOther(v);

			if (EpsTab.arcExists(x,y))
				increaseOrCreate(t,v->getId(),1);
			
			},[&done,&a1](const Algora::Arc* a_now) 
			{
				if (done == false)
					done = (a1 == a_now);
				return done;
			}); 
		});
}
