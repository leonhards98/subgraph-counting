#pragma once

#include "SubgraphAuxCounts.h"
#include "EpsilonTab.h"
#include "graph.dyn/dynamicdigraph.h"
#include "graph/arc.h"
#include "graph/vertex.h"
#include "graph/vertexpair.h"
#include <boost/unordered/unordered_map_fwd.hpp>
#include <functional>
#include <tuple>
#include <utility>
#include <vector>
#include <iostream>
#include "flat_hash_map/bytell_hash_map.hpp"

/**
 * @brief Manages all auxiliary counts. 
 * 
 */
class StructureCounts : public SubgraphAuxCounts
{
public:
	EpsilonTab EpsTab;
	std::vector<int> OnVertexUpId;
	std::vector<int> OnVertexDownId;
	std::vector<int> OnEpsArcAddId;
	std::vector<int> OnEpsArcRemoveId;

	ska::bytell_hash_map<int, int> vLV;
	ska::bytell_hash_map<std::pair<int, int>, int> uLv;
	ska::bytell_hash_map<std::pair<int, int>, int> uLv2;
	ska::bytell_hash_map<int, int> t;
	ska::bytell_hash_map<std::pair<int, int>, int> uLLv;
	ska::bytell_hash_map<std::pair<int, int>, int> cLV;
	ska::bytell_hash_map<std::pair<int, int>, int> pLL;
	ska::bytell_hash_map<std::pair<int, int>, int> uHv;
	ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> cL;
	ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> cL2;
	bool objectives [10];

	friend class SubgraphCounts;

	bool recompute(bool arcAdd);

	void vLVVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<int, int> &, int, int));
	void vLVArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int), bool recompute_after_removed);

	void uLvVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int), ska::bytell_hash_map<std::pair<int, int>, int> &map);
	void uLvArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int), ska::bytell_hash_map<std::pair<int, int>, int> &map, int EpsNr);

	void tVertexToHigh(const Algora::Vertex *v);
	void tVertexToLow(const Algora::Vertex *v);
	void tArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int));

	void uLLvVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int));
	void uLLvArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int));

	void cLVVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int));
	void cLVArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int), bool recompute_after_removed);

	void pLLVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int));
	void pLLArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int));

	void uHvVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int));
	void uHvArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int));

	void cLVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> &, int first, int second, int third, int), ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> &map);
	void cLArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> &, int first, int second, int third, int), ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> &map, int EpsNr);

public:
	bool createEpsilonTable(const double &epsilon, const double &epsilon2);
	void setObjectives(bool vLV = true, bool t = true, bool uLv = true, bool uLv2 = true, bool uLLv = true, bool cLV = true, bool pLL = true, bool uHv = true, bool cL1 = true, bool cL2 = true);
	bool prepare();
	void cleanMaps();
	void cleanup();

	/**
	 * @brief Get the number of vLV structures for the given anchor vertices.
	 * 
	 * @param v anchor vertex
	 * @return int 
	 */
	int getNrvLV(const Algora::Vertex *v)
	{
		auto it = vLV.find(v->getId());
		return (it == vLV.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure vLV and their counts.
	 * 
	 * @return auto 
	 */
	auto getvLVbegin() const
	{
		return vLV.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure vLV and their counts.
	 * 
	 * @return auto 
	 */
	auto getvLVend() const
	{
		return vLV.end();
	}

	/**
	 * @brief Get the number of t structures for the given anchor vertices.
	 * 
	 * @param v anchor vertex
	 * @return int 
	 */
	int getNrt(const Algora::Vertex *v)
	{
		auto it = t.find(v->getId());
		return (it == t.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure t and their counts.
	 * 
	 * @return auto 
	 */
	auto gettbegin() const
	{
		return t.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure t and their counts.
	 * 
	 * @return auto 
	 */
	auto gettend() const
	{
		return t.end();
	}

	/**
	 * @brief Get the number of uLv structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNruLv(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = uLv.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = uLv.find({id1, id2});
		else
			it = uLv.find({id2, id1});

		return (it == uLv.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure uLv and their counts.
	 * 
	 * @return auto 
	 */
	auto getuLvbegin() const
	{
		return uLv.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure uLv and their counts.
	 * 
	 * @return auto 
	 */
	auto getuLvend() const
	{
		return uLv.end();
	}

	/**
	 * @brief Get the number of uLv structures for the given anchor vertices
	 * using the second epsilon partition.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNruLv2(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = uLv2.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = uLv2.find({id1, id2});
		else
			it = uLv2.find({id2, id1});

		return (it == uLv2.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure uLv and their counts using the second epsilon partition.
	 * 
	 * @return auto 
	 */
	auto getuLvbegin2() const
	{
		return uLv2.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure uLv and their counts using the second epsilon partition.
	 * 
	 * @return auto 
	 */
	auto getuLvend2() const
	{
		return uLv2.end();
	}

	/**
	 * @brief Get the number of uLLv structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNruLLv(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = uLLv.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = uLLv.find({id1, id2});
		else
			it = uLLv.find({id2, id1});

		return (it == uLLv.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure uLLv and their counts.
	 * 
	 * @return auto 
	 */
	auto getuLLvbegin() const
	{
		return uLLv.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure uLLv and their counts.
	 * 
	 * @return auto 
	 */
	auto getuLLvend() const
	{
		return uLLv.end();
	}

	/**
	 * @brief Get the number of cLV structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNrcLV(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = cLV.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = cLV.find({id1, id2});
		else
			it = cLV.find({id2, id1});

		return (it == cLV.end() ? 0 : it->second);
	}
	/**
	 * @brief Get the number of cLV structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNrcLV(const int id1, const int id2)
	{
		auto it = cLV.end();

		if (id1 < id2)
			it = cLV.find({id1, id2});
		else
			it = cLV.find({id2, id1});

		return (it == cLV.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure cLV and their counts.
	 * 
	 * @return auto 
	 */
	auto getcLVbegin() const
	{
		return cLV.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure cLV and their counts.
	 * 
	 * @return auto 
	 */
	auto getcLVend() const
	{
		return cLV.end();
	}

	/**
	 * @brief Get the number of pLL structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNrpLL(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = pLL.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = pLL.find({id1, id2});
		else
			it = pLL.find({id2, id1});

		return (it == pLL.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure pLL and their counts.
	 * 
	 * @return auto 
	 */
	auto getpLLbegin() const
	{
		return pLL.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure pLL and their counts.
	 * 
	 * @return auto 
	 */
	auto getpLLend() const
	{
		return pLL.end();
	}

	/**
	 * @brief Get the number of uHv structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @return int 
	 */
	int getNruHv(const Algora::Vertex *v1, const Algora::Vertex *v2)
	{
		auto it = uHv.end();
		int id1 = v1->getId();
		int id2 = v2->getId();

		if (id1 < id2)
			it = uHv.find({id1, id2});
		else
			it = uHv.find({id2, id1});

		return (it == uHv.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure uHv and their counts.
	 * 
	 * @return auto 
	 */
	auto getuHvbegin() const
	{
		return uHv.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure uHv and their counts.
	 * 
	 * @return auto 
	 */
	auto getuHvend() const
	{
		return uHv.end();
	}

	/**
	 * @brief Get the number of cL structures for the given anchor vertices.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @param v3 third anchor vertex
	 * @return int 
	 */
	int getNrcL(const Algora::Vertex *v1, const Algora::Vertex *v2, const Algora::Vertex *v3)
	{
		auto it = cL.end();
		int id1 = v1->getId();
		int id2 = v2->getId();
		int id3 = v3->getId();

		if (id1 <= id2 && id2 <= id3)
			it = cL.find({id1, id2, id3});
		else if (id1 <= id3 && id3 <= id2)
			it = cL.find({id1, id3, id2});
		else if (id2 <= id1 && id1 <= id3)
			it = cL.find({id2, id1, id3});
		else if (id2 <= id3 && id3 <= id1)
			it = cL.find({id2, id3, id1});
		else if (id3 <= id1 && id1 <= id2)
			it = cL.find({id3, id1, id2});
		else if (id3 <= id2 && id2 <= id1)
			it = cL.find({id3, id2, id1});

		return (it == cL.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure cL and their counts.
	 * 
	 * @return auto 
	 */
	auto getcLbegin() const
	{
		return cL.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure cL and their counts.
	 * 
	 * @return auto 
	 */
	auto getcLend() const
	{
		return cL.end();
	}

	/**
	 * @brief Get the number of cL structures for the given anchor vertices
	 * using the second epsilon partition.
	 * The anchor vertices can be given in any order.
	 * 
	 * @param v1 first anchor vertex
	 * @param v2 second anchor vertex
	 * @param v3 third anchor vertex
	 * @return int 
	 */
	int getNrcL2(const Algora::Vertex *v1, const Algora::Vertex *v2, const Algora::Vertex *v3)
	{
		auto it = cL2.end();
		int id1 = v1->getId();
		int id2 = v2->getId();
		int id3 = v3->getId();

		if (id1 <= id2 && id2 <= id3)
			it = cL2.find({id1, id2, id3});
		else if (id1 <= id3 && id3 <= id2)
			it = cL2.find({id1, id3, id2});
		else if (id2 <= id1 && id1 <= id3)
			it = cL2.find({id2, id1, id3});
		else if (id2 <= id3 && id3 <= id1)
			it = cL2.find({id2, id3, id1});
		else if (id3 <= id1 && id1 <= id2)
			it = cL2.find({id3, id1, id2});
		else if (id3 <= id2 && id2 <= id1)
			it = cL2.find({id3, id2, id1});

		return (it == cL2.end() ? 0 : it->second);
	}
	/**
	 * @brief Get begin-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure cL and their counts using the second epsilon partition.
	 * 
	 * @return auto 
	 */
	auto getcLbegin2() const
	{
		return cL2.begin();
	}
	/**
	 * @brief Get end-iterator of the hahs map containing all vertices which are anchor
	 * vertices of the the auxiliary structure cL and their counts using the second epsilon partition.
	 * 
	 * @return auto 
	 */
	auto getcLend2() const
	{
		return cL2.end();
	}

	void printEpsTable()
	{
		EpsTab.print_table();
	}

private:
	/**
	 * @brief Reduces the value of the element of the given hash map defined by the given key. If the decrement is larger than the value, deletes the entry.
	 * The elemnts of the key can be given in any order.
	 * 
	 * @param map hash map to delete from
	 * @param first one part of the key
	 * @param second one part of the key
	 * @param decrement number by which to decrease
	 */
	static void reduceOrDelete(ska::bytell_hash_map<std::pair<int, int>, int> &map, int first, int second, int decrement)
	{
		if (decrement < 1)
			return;

		auto it = map.end();

		if (first <= second)
			it = map.find({first, second});
		else
			it = map.find({second, first});

		if (it == map.end())
			return;
		if (it->second <= decrement)
		{
			map.erase(it);
			map.shrink_to_fit();
		}
		else
			it->second -= decrement;
	}
	/**
	 * @brief Reduces the value of the element of the given hash map defined by the given key. If the decrement is larger than the value, deletes the entry.
	 * The elemnts of the key can be given in any order.
	 * 
	 * @param map hash map to delete from
	 * @param first one part of the key
	 * @param second one part of the key
	 * @param third one part of the key
	 * @param decrement number by which to decrease
	 */
	static void reduceOrDelete(ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> &map, int first, int second, int third, int decrement)
	{

		if (decrement < 1)
			return;

		auto it = map.end();

		if (first <= second && second <= third)
			it = map.find({first, second, third});
		else if (first <= third && third <= second)
			it = map.find({first, third, second});
		else if (second <= first && first <= third)
			it = map.find({second, first, third});
		else if (second <= third && third <= first)
			it = map.find({second, third, first});
		else if (third <= first && first <= second)
			it = map.find({third, first, second});
		else if (third <= second && second <= first)
			it = map.find({third, second, first});

		if (it == map.end())
			return;
		if (it->second <= decrement)
		{
			map.erase(it);
			map.shrink_to_fit();
		}
		else
			it->second -= decrement;
	}
	/**
	 * @brief Reduces the value of the element of the given hash map defined by the given key. 
	 * If the decrement is larger than the value, deletes the entry.
	 * 
	 * @param map hash map to delete from
	 * @param key key to the element that is reduced
	 * @param decrement number by which to decrease
	 */
	static void reduceOrDelete(ska::bytell_hash_map<int, int> &map, int key, int decrement)
	{
		if (decrement < 1)
			return;
		auto it = map.find(key);
		if (it == map.end())
			return;
		if (it->second <= decrement)
		{
			map.erase(it);
			map.shrink_to_fit();
		}
		else
			it->second -= decrement;
	}

	/**
	 * @brief Increases the value of the element of the given hash map defined by the given key. If the key doesn't exist, create a new entry.
	 * The elemnts of the key can be given in any order.
	 * 
	 * @param map hash map to delete from
	 * @param first one part of the key
	 * @param second one part of the key
	 * @param increment number by which to increase
	 */
	static void increaseOrCreate(ska::bytell_hash_map<std::pair<int, int>, int> &map, int first, int second, int increment)
	{
		if (increment <= 0)
			return;

		if (first <= second)
			map[{first, second}] += increment;

		else
			map[{second, first}] += increment;
	}
	/**
	 * @brief Increases the value of the element of the given hash map defined by the given key. If the key doesn't exist, create a new entry.
	 * The elemnts of the key can be given in any order.
	 * 
	 * @param map hash map to delete from
	 * @param first one part of the key
	 * @param second one part of the key
	 * @param third one part of the key
	 * @param increment number by which to increase
	 */
	static void increaseOrCreate(ska::bytell_hash_map<std::tuple<int, int, int>, int, boost::hash<std::tuple<int, int, int>>> &map, int first, int second, int third, int increment)

	{
		if (increment <= 0)
			return;
		if (first <= second && second <= third)
			map[{first, second, third}] += increment;
		else if (first <= third && third <= second)
			map[{first, third, second}] += increment;
		else if (second <= first && first <= third)
			map[{second, first, third}] += increment;
		else if (second <= third && third <= first)
			map[{second, third, first}] += increment;
		else if (third <= first && first <= second)
			map[{third, first, second}] += increment;
		else if (third <= second && second <= first)
			map[{third, second, first}] += increment;
	}

	/**
	 * @brief Increases the value of the element of the given hash map defined by the given key. If the key doesn't exist, create a new entry.
	 * 
	 * @param map hash map to delete from
	 * @param key key to the element that is reduced
	 * @param increment number by which to increase
	 */
	static void increaseOrCreate(ska::bytell_hash_map<int, int> &map, int key, int increment)
	{
		if (increment <= 0)
			return;

		map[key] += increment;
	}
};
