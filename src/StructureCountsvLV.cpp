#include "StructureCounts.h"

/**
 * @brief updates the uLV-auxiliary count by calculating the number the count should change by when inserting/removing the given arc. 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the arc is added or deleted
 * 
 * @param a arc that is added/deleted
 * @param operation function that either increments or decrements the a given map. This should decrement if the arc is removed and increment if 
 * the arc in inserted
 */
void StructureCounts::vLVVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<int, int> &, int, int))
{
	int deg_v{0};
	graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *)
	{ deg_v++; });

	graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a)
	{
		auto w = a->getOther(v);
		operation(vLV,w->getId(),deg_v-1); 
	});
}

/**
 * @brief updates the vLV-auxiliary count by calculating the number the count should change by when the given vertex changes from one epsilon partition to the other 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the vertex changes from low to high or the other way around
 * 
 * @param v vertex that changes partition
 * @param operation function that either increments or decrements the a given map. This should decrement if the vertex becomes high degree and increment if 
 * the vertex becomes low degree
 */
void StructureCounts::vLVArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int), bool recompute_after_removed)
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();

	int deg_h{0}, deg_t{0};

	if (recompute_after_removed)
	{
		graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *)
		{ deg_h++; });

		graph->getDiGraph()->mapIncidentArcs(tail, [&](const Algora::Arc *)
		{ deg_t++; });
	}
	else
	{
		deg_h = graph->getDiGraph()->getDegree(head, 0);
		deg_t = graph->getDiGraph()->getDegree(tail, 0);
	}

	if (EpsTab.is_low_deg2(head))
	{

		graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *a_head)
		{
			auto other = a_head->getOther(head);
			if (other == tail)
				return;
			operation(vLV, other->getId(), 1);
		});

		operation(vLV, tail->getId(), deg_h - 1);
	}
	if (EpsTab.is_low_deg2(tail))
	{
		graph->getDiGraph()->mapIncidentArcs(tail, [&](const Algora::Arc *a_tail)
		{
			auto other = a_tail->getOther(tail);
			if (other == head)
				return;
			operation(vLV, other->getId(), 1);
		});

		operation(vLV, head->getId(), deg_t - 1);
	}
}
