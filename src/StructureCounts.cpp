#include "StructureCounts.h"
#include "graph.dyn/dynamicdigraph.h"
#include "graph/arc.h"
#include "graph/graph_functional.h"
#include "graph/vertex.h"
#include <boost/unordered/unordered_map_fwd.hpp>
#include <iostream>

/**
 * @brief Creates two epsilon tabels with the given parameters 
 * 
 * @param epsilon parameter for the first table used for subgraphs with a theoretical value of 0.33
 * @param epsilon2 parameter for the second table used for subgraphs with a theoretical value of 0.5
 */
bool StructureCounts::createEpsilonTable(const double &epsilon, const double &epsilon2)
{
	if (!hasGraph())
	{
		std::cerr << "Need to set Graph before creation Epsilon Table\n";
		return 0;
	}
	this->EpsTab = EpsilonTab();
	if (!EpsTab.setEpsilon(epsilon,epsilon2))
		return 0;
	EpsTab.setGraph(graph);
	if (!EpsTab.prepare())
		return 0;
	EpsTab.setOnRecompute([&](bool arcAdd){this->cleanMaps(); this->recompute(arcAdd);});
	return 1;

}


/**
 * @brief Recompute all auxiliary counts by first deactivating all arcs and then reactivating them and
 * calculating the auxiliary counts accordingly
 * 
 * @param arcAdd if the recomputation happens at a step where an arc is added
 * @return true 
 * @return false 
 */
bool StructureCounts::recompute(bool arcAdd)
{

	EpsTab.fixTable();
	
	
	for (auto it = EpsTab.insertedArcs.begin(); it != EpsTab.insertedArcs.end(); it++)
		graph->getDiGraph()->deactivateArc(it->first);

	

	for (auto it = EpsTab.insertedArcs.begin(); it != EpsTab.insertedArcs.end(); it++)
	{


		graph->getDiGraph()->activateArc(it->first);
		if (objectives[0] == true)
			vLVArcChange(it->first,increaseOrCreate,!arcAdd);
		if (objectives[2] == true)
			uLvArcChange(it->first,increaseOrCreate,uLv,1);
		if (objectives[3] == true)
			uLvArcChange(it->first,increaseOrCreate,uLv2,2);
		if (objectives[1] == true)
			tArcChange(it->first,increaseOrCreate);
		if (objectives[4] == true)
			uLLvArcChange(it->first,increaseOrCreate);
		if (objectives[5] == true)
			cLVArcChange(it->first,increaseOrCreate,!arcAdd);
		if (objectives[6] == true)
			pLLArcChange(it->first,increaseOrCreate);
		if (objectives[7] == true)
			uHvArcChange(it->first,increaseOrCreate);
		if (objectives[8] == true)
			cLArcChange(it->first,increaseOrCreate, cL, 1);
		if (objectives[9] == true)
			cLArcChange(it->first,increaseOrCreate, cL2, 2);
		
	}



	EpsTab.unfixTable();


	return 1;

}
/**
 * @brief Set the objectives.
 * This means define which auxiliary counts need to be maintained.
 * 
 */
void StructureCounts::setObjectives(bool vLV, bool t, bool uLv1, bool uLv2, bool uLLv, bool cLV, bool pLL, bool uHv, bool cL1, bool cL2)
{
	if (vLV)
		objectives[0] = true;
	else
		objectives[0] = false;
	if (t)
		objectives[1] = true;
	else
		objectives[1] = false;
	if (uLv1)
		objectives[2] = true;
	else
		objectives[2] = false;
	if (uLv2)
		objectives[3] = true;
	else
		objectives[3] = false;
	if (uLLv)
		objectives[4] = true;
	else
		objectives[4] = false;
	if (cLV)
		objectives[5] = true;
	else
		objectives[5] = false;
	if (pLL)
		objectives[6] = true;
	else
		objectives[6] = false;
	if (uHv)
		objectives[7] = true;
	else
		objectives[7] = false;
	if (cL1)
		objectives[8] = true;
	else
		objectives[8] = false;
	if (cL2)
		objectives[9] = true;
	else
		objectives[9] = false;
}

/**
 * @brief Prepare by creating all observers of the given graph that maintain the auxiliary counts.
 * That includes whenever an arc is addded/removed and whenever a vertex changes partition
 * 
 */
bool StructureCounts::prepare()
{
	
	if (!hasGraph() || !EpsTab.hasGraph())
	{
		std::cerr << "Need to set Graph and EpsilonTable before preparing\n";
		return 0;
	}


	//Fill vLV
	if (objectives[0] == true)
	{
		EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(0), [&](Algora::Arc *a) { vLVArcChange(a,increaseOrCreate,false);});
		EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(0), [&](Algora::Arc *a) { vLVArcChange(a,reduceOrDelete,false);});
		EpsTab.onVertexToHigh2(&OnVertexUpId.emplace_back(0), [&](Algora::Vertex *v) { vLVVertexChange(v,reduceOrDelete);});
		EpsTab.onVertexToLow2(&OnVertexDownId.emplace_back(0), [&](Algora::Vertex *v) { vLVVertexChange(v,increaseOrCreate);});
	}
	
	//Fill uLv
	if (objectives[2] == true)
	{
		EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(1), [&](Algora::Arc *a) { uLvArcChange(a,increaseOrCreate, uLv, 1);});
		EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(1), [&](Algora::Arc *a) { uLvArcChange(a,reduceOrDelete, uLv, 1);});
		EpsTab.onVertexToHigh1(&OnVertexUpId.emplace_back(1), [&](Algora::Vertex *v) { uLvVertexChange(v,reduceOrDelete, uLv);});
		EpsTab.onVertexToLow1(&OnVertexDownId.emplace_back(1), [&](Algora::Vertex *v) { uLvVertexChange(v,increaseOrCreate, uLv);});

	}
	if (objectives[3] == true)
	{
		EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(1), [&](Algora::Arc *a) { uLvArcChange(a,increaseOrCreate, uLv2, 2);});
		EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(1), [&](Algora::Arc *a) { uLvArcChange(a,reduceOrDelete, uLv2, 2);});

		EpsTab.onVertexToHigh2(&OnVertexUpId.emplace_back(1), [&](Algora::Vertex *v) { uLvVertexChange(v,reduceOrDelete, uLv2);});
		EpsTab.onVertexToLow2(&OnVertexDownId.emplace_back(1), [&](Algora::Vertex *v) { uLvVertexChange(v,increaseOrCreate, uLv2);});
	}


	//Fill t (needs to be after uLv)
	if (objectives[1] == true)
	{
		EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(2), [&](Algora::Arc *a) { tArcChange(a,increaseOrCreate);});
		EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(2), [&](Algora::Arc *a) { tArcChange(a,reduceOrDelete);});
		EpsTab.onVertexToHigh1(&OnVertexUpId.emplace_back(2), [&](Algora::Vertex *v) { tVertexToHigh(v);});
		EpsTab.onVertexToLow1(&OnVertexDownId.emplace_back(2), [&](Algora::Vertex *v) { tVertexToLow(v);});
	}

	//Fill uLLv
	if (objectives[4] == true)
	{
		EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(3), [&](Algora::Arc *a) { uLLvArcChange(a,increaseOrCreate);});
		EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(3), [&](Algora::Arc *a) { uLLvArcChange(a,reduceOrDelete);});
		EpsTab.onVertexToHigh1(&OnVertexUpId.emplace_back(3), [&](Algora::Vertex *v) { uLLvVertexChange(v,reduceOrDelete);});
		EpsTab.onVertexToLow1(&OnVertexDownId.emplace_back(3), [&](Algora::Vertex *v) { uLLvVertexChange(v,increaseOrCreate);});
	}
	
	//Fill cLV
	if (objectives[5] == true)
	{
		EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(4), [&](Algora::Arc *a) { cLVArcChange(a,increaseOrCreate,false);});
		EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(4), [&](Algora::Arc *a) { cLVArcChange(a,reduceOrDelete,false);});
		EpsTab.onVertexToHigh1(&OnVertexUpId.emplace_back(4), [&](Algora::Vertex *v) { cLVVertexChange(v,reduceOrDelete);});
		EpsTab.onVertexToLow1(&OnVertexDownId.emplace_back(4), [&](Algora::Vertex *v) { cLVVertexChange(v,increaseOrCreate);});
	}
	
	//Fill pLL
	if (objectives[6] == true)
	{
		EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(5), [&](Algora::Arc *a) { pLLArcChange(a,increaseOrCreate);});
		EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(5), [&](Algora::Arc *a) { pLLArcChange(a,reduceOrDelete);});
		EpsTab.onVertexToHigh1(&OnVertexUpId.emplace_back(5), [&](Algora::Vertex *v) { pLLVertexChange(v,reduceOrDelete);});
		EpsTab.onVertexToLow1(&OnVertexDownId.emplace_back(5), [&](Algora::Vertex *v) { pLLVertexChange(v,increaseOrCreate);});
	}

	//Fill uHv
	if (objectives[7] == true)
	{
		EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(6), [&](Algora::Arc *a) { uHvArcChange(a,increaseOrCreate);});
		EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(6), [&](Algora::Arc *a) { uHvArcChange(a,reduceOrDelete);});
		EpsTab.onVertexToHigh1(&OnVertexUpId.emplace_back(6), [&](Algora::Vertex *v) { uHvVertexChange(v,increaseOrCreate);});
		EpsTab.onVertexToLow1(&OnVertexDownId.emplace_back(6), [&](Algora::Vertex *v) { uHvVertexChange(v,reduceOrDelete);});
	}
	
	//Fill cL
	if (objectives[8] == true)
	{
		EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(7), [&](Algora::Arc *a) {cLArcChange(a,increaseOrCreate, cL, 1);});
		EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(7), [&](Algora::Arc *a) { cLArcChange(a,reduceOrDelete, cL, 1);});
		EpsTab.onVertexToHigh1(&OnVertexUpId.emplace_back(7), [&](Algora::Vertex *v) { cLVertexChange(v,reduceOrDelete, cL);});
		EpsTab.onVertexToLow1(&OnVertexDownId.emplace_back(7), [&](Algora::Vertex *v) { cLVertexChange(v,increaseOrCreate, cL);});

	}
	if (objectives[9] == true)
	{
		EpsTab.onArcAdd(&OnEpsArcAddId.emplace_back(7), [&](Algora::Arc *a) { cLArcChange(a,increaseOrCreate, cL2, 2);});
		EpsTab.onArcRemove(&OnEpsArcRemoveId.emplace_back(7), [&](Algora::Arc *a) { cLArcChange(a,reduceOrDelete, cL2, 2);});

		EpsTab.onVertexToHigh2(&OnVertexUpId.emplace_back(7), [&](Algora::Vertex *v) { cLVertexChange(v,reduceOrDelete, cL2);});
		EpsTab.onVertexToLow2(&OnVertexDownId.emplace_back(7), [&](Algora::Vertex *v) { cLVertexChange(v,increaseOrCreate, cL2);});
	}
	return 1;
}


/**
 * @brief Clear all auxiliary counts
 * 
 */
void StructureCounts::cleanMaps()
{
	this->vLV.clear();
	this->uLv.clear();
	this->uLv2.clear();
	this->t.clear();
	this->uLLv.clear();
	this->cLV.clear();
	this->pLL.clear();
	this->uHv.clear();
	this->cL.clear();
	this->cL2.clear();
		
}
/**
 * @brief Remove all observers
 * 
 */
void StructureCounts::cleanup()
{
	cleanMaps();
	for (auto &id : OnVertexUpId)
		EpsTab.removeOnVertexToHigh1(&id);
	for (auto &id : OnVertexDownId)
		EpsTab.removeOnVertexToLow1(&id);
	for (auto &id : OnEpsArcAddId)
		EpsTab.removeOnArcAdd(&id);
	for (auto &id : OnEpsArcRemoveId)
		EpsTab.removeOnArcRemove(&id);

}

