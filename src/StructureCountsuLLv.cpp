#include "StructureCounts.h"
#include "algorithm/valuecomputingalgorithm.h"
#include "graph/arc.h"
#include "graph/vertex.h"
#include <vector>

/**
 * @brief updates the uLLv-auxiliary count by calculating the number the count should change by when inserting/removing the given arc. 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the arc is added or deleted
 * 
 * @param a arc that is added/deleted
 * @param operation function that either increments or decrements the a given map. This should decrement if the arc is removed and increment if 
 * the arc in inserted
 * @param recompute_after_removed set to true if this operation is part of a recomputation after an arc was removed
 */
void StructureCounts::uLLvArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	bool head_low = EpsTab.is_low_deg(head);
	bool tail_low = EpsTab.is_low_deg(tail);

	if (head_low)
	{
		graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *a_head)
		{
			auto other_a_head = a_head->getOther(head);
			if (other_a_head == tail || EpsTab.is_high_deg(other_a_head))
				return;

			graph->getDiGraph()->mapIncidentArcs(other_a_head,[&](const Algora::Arc *a_head2)
			{
				auto other_a_head2 = a_head2->getOther(other_a_head);
				if (other_a_head2 == head || other_a_head2 == tail)
					return;

				operation(uLLv,tail->getId(),other_a_head2->getId(),1);

			}); 
		});
	}

	if (tail_low)
	{
		graph->getDiGraph()->mapIncidentArcs(tail, [&](const Algora::Arc *a_tail)
		{
			auto other_a_tail = a_tail->getOther(tail);
			if (other_a_tail == head || EpsTab.is_high_deg(other_a_tail))
				return;

			graph->getDiGraph()->mapIncidentArcs(other_a_tail, [&](const Algora::Arc *a_tail2)
			{
				auto other_a_tail2 = a_tail2->getOther(other_a_tail);
				if (other_a_tail2 == head || other_a_tail2 == tail)
					return;

				operation(uLLv, head->getId(), other_a_tail2->getId(), 1);
			});
		});
	}

	if (head_low && tail_low)
	{

		graph->getDiGraph()->mapIncidentArcs(head, [&](const Algora::Arc *a_head)
		{
			auto other_head = a_head->getOther(head);
			if (other_head == tail)
				return;

			graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a_tail)
			{
				auto other_tail = a_tail->getOther(tail);
				if (other_tail == head || other_head == other_tail || other_head == other_tail)
					return;

				operation(uLLv,other_head->getId(),other_tail->getId(),1);
			}); 
		});
	}
}

/**
 * @brief updates the uLLv-auxiliary count by calculating the number the count should change by when the given vertex changes from one epsilon partition to the other 
 * This is then given to the given function that either increases or
 * decreases the count by that number. This depends on if the vertex changes from low to high or the other way around
 * 
 * @param v vertex that changes partition
 * @param operation function that either increments or decrements the a given map. This should decrement if the vertex becomes high degree and increment if 
 * the vertex becomes low degree
 */
void StructureCounts::uLLvVertexChange(const Algora::Vertex *v, void(operation)(ska::bytell_hash_map<std::pair<int, int>, int> &, int first, int second, int))
{
	Algora::Vertex *x;
	Algora::Vertex *y;

	std::vector<Algora::Vertex *> w_vec;

	graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a3)
	{ w_vec.push_back(a3->getOther(v)); });

	graph->getDiGraph()->mapIncidentArcs(v, [&](const Algora::Arc *a1)
	{
		x = a1->getOther(v);
		if (EpsTab.is_high_deg(x))
			return;
		graph->getDiGraph()->mapIncidentArcs(x, [&](const Algora::Arc *a2)
		{
			y = a2->getOther(x);
			if (y == v)
				return;

			for (auto w : w_vec)
			{
				if (w == x)
					continue;

				operation(uLLv, w->getId(), y->getId(), 1);
			}
		});
	});
}
