#pragma once

#include <vector>
#include <memory>
#include <string>
#include <boost/unordered/unordered_map_fwd.hpp>
#include "graph.dyn/dynamicdigraph.h"
#include "graph/vertex.h"


class SubgraphAuxCounts
{
protected:
	Algora::DynamicDiGraph *graph;
	std::vector<int> OnVertexAddId;
	std::vector<int> OnVertexRemoveId;
	std::vector<int> OnArcAddId;
	std::vector<int> OnArcRemoveId;

	virtual void cleanup() {}

public:
	SubgraphAuxCounts() : graph(nullptr){};
	void setGraph(Algora::DynamicDiGraph *InGraph)
	{
		this->graph = InGraph;
	}
	virtual bool prepare() = 0;
	bool unsetGraph()
	{
		if (!hasGraph())
			return 1;
		for (auto &id : OnVertexAddId)
			graph->getDiGraph()->removeOnVertexAdd(&id);
		for (auto &id : OnVertexRemoveId)
			graph->getDiGraph()->removeOnVertexRemove(&id);
		for (auto &id : OnArcAddId)
			graph->getDiGraph()->removeOnArcAdd(&id);
		for (auto &id : OnArcRemoveId)
			graph->getDiGraph()->removeOnArcRemove(&id);
		cleanup();
		graph = nullptr;

		return 1;
	}

	bool hasGraph() { return graph != nullptr; }

	virtual ~SubgraphAuxCounts()
	{
		if (!hasGraph())
			return;
		this->unsetGraph();
	}
};
