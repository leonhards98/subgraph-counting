#include "SubgraphCounts.h"
#include "graph.incidencelist/incidencelistvertex.h"
#include "graph/arc.h"
#include "graph/vertex.h"
#include <iostream>
#include <cmath>
#include <boost/math/special_functions/binomial.hpp>
#include <boost/math/special_functions/math_fwd.hpp>

/**
 * @brief Sets the epsilon values for the two partitions
 * 
 * @param epsilon1 used for all auxiliary counts with theroetical value 0.33
 * @param epsilon2 used for all auxiliary counts with theroetical value 0.5
 */
void SubgraphCounts::setEpsilon(double epsilon1, double epsilon2)
{
	this->epsilon1 = epsilon1;
	this->epsilon2 = epsilon2;
}
/**
 * @brief Defines which sunbgraphs should be maintained and other options for the algorithm
 * 
 * @param observedVertices vector with the vertices for which the counts should be maintained. If it's empty, all will be considered
 * @param tCycles 
 * @param tPaths 
 * @param claws 
 * @param paws 
 * @param fCycles 
 * @param diamonds 
 * @param fCliques 
 * @param total_counts if the total counts should be maintained
 * @param s_counts if the s-counts should be maintained
 */
void SubgraphCounts::setObjectives(std::vector<std::string> &observedVertices, bool tCycles, bool tPaths, bool claws, bool paws, bool fCycles, bool diamonds, bool fCliques, bool total_counts, bool s_counts)
{
	for (auto v : observedVertices)
	{
		this->observedVertices[std::stoi(v)] = true;
	}
	this->objectives[0] = tCycles || paws;
	this->objectives[1] = tPaths;
	this->objectives[2] = claws;
	this->objectives[3] = paws;
	this->objectives[4] = fCycles;
	this->objectives[5] = diamonds;
	this->objectives[6] = fCliques;
	this->objectives[7] = total_counts;
	this->objectives[8] = s_counts;

}

/**
 * @brief prepares the algorthm by intorducing all necessary observers accoring to the set options.
 * First, the observers for maintaining the auxiliary counts when an arc is removed are set.
 * Then the ones that calculate the changes in the subgraph counts.
 * Finally the ones that maintain the auxiliary counts when an arc in added are set.
 * 
 * This ensures the correct order of operations.
 */
bool SubgraphCounts::prepare()
{
	if (!hasGraph())
	{
		std::cerr << "Need to set Graph before preparing.\n";
		return 0;
	}

	if (epsilon1 == -1 || epsilon2 == -1)
	{

		std::cerr << "Need to set Epsilon before preparing\n";
		return 0;
	}

	//Here, the dependencies between the subgraph counts and the necessary auxiliary counts are defined.
	bool vLV{objectives[1]};
	bool t{objectives[3]};
	bool uLv1{t || objectives[0] || objectives[3] || objectives[4] || objectives[5]};
	bool uLv2{t || objectives[0] || objectives[1] || objectives[3]};
	bool uLLv{objectives[4]};
	bool cLV{objectives[3]};
	bool pLL{objectives[5]};
	bool uHv{objectives[4] || objectives[5]};
	bool cL1{objectives[5] || objectives[3]};
	bool cL2{objectives[6]};


	//Initialize the auxiliary counts
	StrucCount.setGraph(graph);
	StrucCount.createEpsilonTable(epsilon1, epsilon2);
	StrucCount.setObjectives(vLV, t, uLv1, uLv2, uLLv, cLV, pLL, uHv, cL1, cL2);

	// Change of aux-counts for removal
	StrucCount.EpsTab.onArcRemove(&StrucCount.OnEpsArcRemoveId.emplace_back(15), [&](Algora::Arc *a)
	{
		if (objectives[8])
		{
			if (objectives[0] == true)
				sTriangleArcChange(a, reduceOrDelete);
			if (objectives[1] == true)
				sTPathArcChange(a, reduceOrDelete);
			if (objectives[2] == true)
				sClawArcChange(a, reduceOrDelete);
			if (objectives[3] == true)
				sPawArcChange(a, reduceOrDelete);
			if (objectives[4] == true)
				sFCycleArcChange(a, reduceOrDelete);
			if (objectives[5] == true)
				sDiamondArcChange(a, reduceOrDelete);
			if (objectives[6] == true)
				sFCliqueArcChange(a, reduceOrDelete);

		    if (observedVertices.count(a->getHead()->getId()) != 0 && graph->getDiGraph()->getDegree(a->getHead(), 0) == 1)
		    {
			auto it = std::find(CurrentObservedVertices.begin(), CurrentObservedVertices.end(), a->getHead());
			CurrentObservedVertices.erase(it);
		    }

		    if (observedVertices.count(a->getTail()->getId()) != 0 && graph->getDiGraph()->getDegree(a->getTail(), 0) == 1)
		    {
			auto it = std::find(CurrentObservedVertices.begin(), CurrentObservedVertices.end(), a->getTail());
			CurrentObservedVertices.erase(it);
		    }
		}

		if (objectives[7])
		{
			if (objectives[0] == true)
				tCycleArcChange(a, subtract);
			if (objectives[1] == true)
				tPathArcChange(a, subtract);
			if (objectives[2] == true)
				ClawArcChange(a, subtract);
			if (objectives[3] == true)
				pawArcChange(a, subtract);
			if (objectives[4] == true)
				fCycleArcChange(a, subtract);
			if (objectives[5] == true)
				DiamondArcChange(a, subtract);
			if (objectives[6] == true)
				fCliqueArcChange(a, subtract);
		}
		
	});

	//Change in subgraph counts for insertion and removal
	StrucCount.prepare();

	// Change of aux-counts for insertion
	StrucCount.EpsTab.onArcAdd(&StrucCount.OnEpsArcAddId.emplace_back(15), [&](Algora::Arc *a)
	{
		if (objectives[8])
		{

            if (observedVertices.count(a->getHead()->getId()) != 0 && graph->getDiGraph()->getDegree(a->getHead(), 0) == 1)
            {
                CurrentObservedVertices.push_back(a->getHead());
            }

            if (observedVertices.count(a->getTail()->getId()) != 0 && graph->getDiGraph()->getDegree(a->getTail(), 0) == 1)
            {
                CurrentObservedVertices.push_back(a->getTail());
            }

			if (objectives[0] == true)
				sTriangleArcChange(a, increaseOrCreate);
			if (objectives[1] == true)
				sTPathArcChange(a, increaseOrCreate);
			if (objectives[2] == true)
				sClawArcChange(a, increaseOrCreate);
			if (objectives[3] == true)
				sPawArcChange(a, increaseOrCreate);
			if (objectives[4] == true)
				sFCycleArcChange(a, increaseOrCreate);
			if (objectives[5] == true)
				sDiamondArcChange(a, increaseOrCreate);
			if (objectives[6] == true)
				sFCliqueArcChange(a, increaseOrCreate);
		}

		if (objectives[7])
		{
			if (objectives[0] == true)
				tCycleArcChange(a, add);
			if (objectives[1] == true)
				tPathArcChange(a, add);
			if (objectives[2] == true)
				ClawArcChange(a, add);
			if (objectives[3] == true)
				pawArcChange(a, add);
			if (objectives[4] == true)
				fCycleArcChange(a, add);
			if (objectives[5] == true)
				DiamondArcChange(a, add);
			if (objectives[6] == true)
				fCliqueArcChange(a, add);
		}
	});

	return 1;
}

/**
 * @brief counts the canges in the total number of claws after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::ClawArcChange(const Algora::Arc *a, void(operation)(int &, int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	operation(claw, getNrClaws_extended(head, tail));
}

/**
 * @brief counts the canges in the total number of four-clique after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::fCliqueArcChange(const Algora::Arc *a, void(operation)(int &, int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	operation(fClique, getNrFCliques_extended(head, tail));
}

/**
 * @brief counts the canges in the total number of diamonds after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::DiamondArcChange(const Algora::Arc *a, void(operation)(int &, int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	operation(diamond, getNrDiamonds_extended(head, tail));
}

/**
 * @brief counts the canges in the total number of three-paths after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::tPathArcChange(const Algora::Arc *a, void(operation)(int &, int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();
	operation(tPath, getNrTPaths_extended(head, tail));
}

/**
 * @brief counts the canges in the total number of three-cycles after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::tCycleArcChange(const Algora::Arc *a, void(operation)(int &, int))
{
	Algora::IncidenceListVertex *head = graph->getDiGraph()->vertexAt(a->getHead()->getId());
	Algora::IncidenceListVertex *tail = graph->getDiGraph()->vertexAt(a->getTail()->getId());;
	operation(tCycle, getNrTriangle_extended(head, tail));
}

/**
 * @brief counts the canges in the total number of paws after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::pawArcChange(const Algora::Arc *a, void(operation)(int &, int))
{
	Algora::Vertex *head = a->getHead();
	Algora::Vertex *tail = a->getTail();

	bool arc_removed = !StrucCount.EpsTab.arcExists(head, tail);
	int tHeadTail = this->getNrTriangle_extended(graph->getDiGraph()->vertexAt(head->getId()), graph->getDiGraph()->vertexAt(tail->getId()));
	int tHead{0}, tTail{0};

	if (arc_removed)
	{
		tHead = getNrTriangle_extended_with_edge(head, head, tail);
		tTail = getNrTriangle_extended_with_edge(tail, head, tail);
	}
	else
	{
		tHead = getNrTriangle_extended(head);
		tTail = getNrTriangle_extended(tail);
	}
	operation(paw, getNrPaws_extended(head, tail, tHead, tTail, tHeadTail));
}

/**
 * @brief counts the canges in the total number of four-cycles after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::fCycleArcChange(const Algora::Arc *a, void(operation)(int &, int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();

	operation(fCycle, getNrfCycles_extended(head, tail));
}

/**
 * @brief counts the canges in the number of three-cycles for s-counts after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * If the vector of vertices that should be observed is not empty, only those will be considered.
 * If the vector is empty, the vertices that are in a close proximity to the arc that is changed are looped through
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::sTriangleArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int))
{
	sTriangle_save.clear();
	Algora::Vertex *head = a->getHead();
	Algora::Vertex *tail = a->getTail();

	if (!observedVertices.empty() && objectives[3] == false)
	{
		for (auto s : this->CurrentObservedVertices)
		{
			operation(sTriangle, s->getId(), sTriangleCount(head, tail, s));
		}
		return;
	}

	operation(sTriangle, head->getId(), sTriangleCount(head, tail, head));
	operation(sTriangle, tail->getId(), sTriangleCount(head, tail, tail)); 
    graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(head);
		if (s == tail)
			return;
		operation(sTriangle, s->getId(), sTriangleCount(head, tail, s)); 
	});

}

/**
 * @brief counts the canges in the number of three-paths for s-counts after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * If the vector of vertices that should be observed is not empty, only those will be considered.
 * If the vector is empty, the vertices that are in a close proximity to the arc that is changed are looped through
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::sTPathArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int))
{

	Algora::Vertex *head = a->getHead();
	Algora::Vertex *tail = a->getTail();

	for (auto s : this->CurrentObservedVertices)
	{
		operation(sTPath, s->getId(), sTPathCount(head, tail, s));
	}
	if (!observedVertices.empty())
	{
		return;
	}
    already_observed.clear();

	operation(sTPath, head->getId(), sTPathCount(head, tail, head));
    already_observed[head->getId()] = true;
	operation(sTPath, tail->getId(), sTPathCount(head, tail, tail));
    already_observed[tail->getId()] = true;
    graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(head);
        if (already_observed.find(s->getId()) == already_observed.end())
        {
			operation(sTPath, s->getId(), sTPathCount(head, tail, s));
            already_observed[s->getId()] = true;
        }
        graph->getDiGraph()->mapIncidentArcs(s,[&](const Algora::Arc *a2)
        {
            auto s2 = a2->getOther(s);
            if (already_observed.find(s2->getId()) == already_observed.end())
            {
				operation(sTPath, s2->getId(), sTPathCount(head, tail, s2));
                already_observed[s2->getId()] = true;
            }
        });
	});


    graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(tail);
        if (already_observed.find(s->getId()) == already_observed.end())
        {
			operation(sTPath, s->getId(), sTPathCount(head, tail, s));
            already_observed[s->getId()] = true;
        }
        graph->getDiGraph()->mapIncidentArcs(s,[&](const Algora::Arc *a2)
        {
            auto s2 = a2->getOther(s);
            if (already_observed.find(s2->getId()) == already_observed.end())
            {
				operation(sTPath, s2->getId(), sTPathCount(head, tail, s2));
                already_observed[s2->getId()] = true;
            }
        });
	});
    already_observed.clear();
}

/**
 * @brief counts the canges in the number of claws for s-counts after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * If the vector of vertices that should be observed is not empty, only those will be considered.
 * If the vector is empty, the vertices that are in a close proximity to the arc that is changed are looped through
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::sClawArcChange(const Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int))
{
	Algora::Vertex *head = a->getHead();
	Algora::Vertex *tail = a->getTail();
	int deg_h = graph->getDiGraph()->getDegree(head, 0) - 1;
	int deg_t = graph->getDiGraph()->getDegree(tail, 0) - 1;

	for (auto s : this->CurrentObservedVertices)
	{
		operation(sClaw, s->getId(), sClawCount(head, tail, s, deg_h, deg_t));
	}
	if (!observedVertices.empty())
	{
		return;
	}

 	already_observed.clear();

	operation(sClaw, head->getId(), sClawCount(head, tail, head, deg_h, deg_t));
    already_observed[head->getId()] = true;
	operation(sClaw, tail->getId(), sClawCount(head, tail, tail, deg_h, deg_t));
    already_observed[tail->getId()] = true;
    graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(head);
        if (already_observed.find(s->getId()) == already_observed.end())
        {
			operation(sClaw, s->getId(), sClawCount(head, tail, s, deg_h, deg_t));
            already_observed[s->getId()] = true;
        }
	});


    graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(tail);
        if (already_observed.find(s->getId()) == already_observed.end())
        {
			operation(sClaw, s->getId(), sClawCount(head, tail, s, deg_h, deg_t));
            already_observed[s->getId()] = true;
        }
	});
    already_observed.clear();
}

/**
 * @brief counts the canges in the number of paws for s-counts after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * If the vector of vertices that should be observed is not empty, only those will be considered.
 * If the vector is empty, the vertices that are in a close proximity to the arc that is changed are looped through
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::sPawArcChange(Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int))
{
	Algora::Vertex *head = a->getHead();
	Algora::Vertex *tail = a->getTail();

	bool arc_removed = !StrucCount.EpsTab.arcExists(head, tail);
	int tHeadTail = this->getNrTriangle_extended(graph->getDiGraph()->vertexAt(head->getId()), graph->getDiGraph()->vertexAt(tail->getId()));
	int tHead{0}, tTail{0};

	if (arc_removed)
	{
		tHead = getNrTriangles_save(head);
		tTail = getNrTriangles_save(tail);
	}
	else
	{
		tHead = getNrTriangles(head);
		tTail = getNrTriangles(tail);
	}

	for (auto s : this->CurrentObservedVertices)
	{
		operation(sPaw, s->getId(), sPawCount(head, tail, s, arc_removed, tHead, tTail, tHeadTail));
	}
	if (!observedVertices.empty())
	{
		return;
	}

    already_observed.clear();

	operation(sPaw, head->getId(), sPawCount(head, tail, head, arc_removed, tHead, tTail, tHeadTail));
    already_observed[head->getId()] = true;
	operation(sPaw, tail->getId(), sPawCount(head, tail, tail, arc_removed, tHead, tTail, tHeadTail));
    already_observed[tail->getId()] = true;
    graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(head);
        if (already_observed.find(s->getId()) == already_observed.end())
        {
			operation(sPaw, s->getId(), sPawCount(head, tail, s, arc_removed, tHead, tTail, tHeadTail));
            already_observed[s->getId()] = true;
        }
        graph->getDiGraph()->mapIncidentArcs(s,[&](const Algora::Arc *a2)
        {
            auto s2 = a2->getOther(s);
            if (already_observed.find(s2->getId()) == already_observed.end())
            {
				operation(sPaw, s2->getId(), sPawCount(head, tail, s2, arc_removed, tHead, tTail, tHeadTail));
                already_observed[s2->getId()] = true;
            }
        });
	});


    graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(tail);
        if (already_observed.find(s->getId()) == already_observed.end())
        {
			operation(sPaw, s->getId(), sPawCount(head, tail, s, arc_removed, tHead, tTail, tHeadTail));
            already_observed[s->getId()] = true;
        }
        graph->getDiGraph()->mapIncidentArcs(s,[&](const Algora::Arc *a2)
        {
            auto s2 = a2->getOther(s);
            if (already_observed.find(s2->getId()) == already_observed.end())
            {
				operation(sPaw, s2->getId(), sPawCount(head, tail, s2, arc_removed, tHead, tTail, tHeadTail));
                already_observed[s2->getId()] = true;
            }
        });
	});
    already_observed.clear();
}


/**
 * @brief counts the canges in the number of four-cycles for s-counts after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * If the vector of vertices that should be observed is not empty, only those will be considered.
 * If the vector is empty, the vertices that are in a close proximity to the arc that is changed are looped through
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::sFCycleArcChange(Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int))
{
	Algora::Vertex *head = a->getHead();
	Algora::Vertex *tail = a->getTail();

	for (auto s : this->CurrentObservedVertices)
	{
		operation(sFCycle, s->getId(), sFCycleCount(head, tail, s));
	}
	if (!observedVertices.empty())
	{
		return;
	}

 	already_observed.clear();

	operation(sFCycle, head->getId(), sFCycleCount(head, tail, head));
    already_observed[head->getId()] = true;
	operation(sFCycle, tail->getId(), sFCycleCount(head, tail, tail));
    already_observed[tail->getId()] = true;
    graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(head);
        if (already_observed.find(s->getId()) == already_observed.end())
        {
			operation(sFCycle, s->getId(), sFCycleCount(head, tail, s));
            already_observed[s->getId()] = true;
        }
	});


    graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(tail);
        if (already_observed.find(s->getId()) == already_observed.end())
        {
			operation(sFCycle, s->getId(), sFCycleCount(head, tail, s));
            already_observed[s->getId()] = true;
        }
	});
    already_observed.clear();

}

/**
 * @brief counts the canges in the number of daimonds for s-counts after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * If the vector of vertices that should be observed is not empty, only those will be considered.
 * If the vector is empty, the vertices that are in a close proximity to the arc that is changed are looped through
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::sDiamondArcChange(Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();

	for (auto s : this->CurrentObservedVertices)
	{
		operation(sDiamond, s->getId(), sDiamondCount(head, tail, s));
	}
	if (!observedVertices.empty())
	{
		return;
	}

 	already_observed.clear();

	operation(sDiamond, head->getId(), sDiamondCount(head, tail, head));
    already_observed[head->getId()] = true;
	operation(sDiamond, tail->getId(), sDiamondCount(head, tail, tail));
    already_observed[tail->getId()] = true;
    graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(head);
        if (already_observed.find(s->getId()) == already_observed.end())
        {
			operation(sDiamond, s->getId(), sDiamondCount(head, tail, s));
            already_observed[s->getId()] = true;
        }
	});


    graph->getDiGraph()->mapIncidentArcs(tail,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(tail);
        if (already_observed.find(s->getId()) == already_observed.end())
        {
			operation(sDiamond, s->getId(), sDiamondCount(head, tail, s));
            already_observed[s->getId()] = true;
        }
	});
    already_observed.clear();
}

/**
 * @brief counts the canges in the number of four-cliques for s-counts after the insertion/deletion of given arc.
 * This change is then given to the given function that should either increase or decrease the 
 * cout by that number depending on if the arc was inserted or deleted
 * If the vector of vertices that should be observed is not empty, only those will be considered.
 * If the vector is empty, the vertices that are in a close proximity to the arc that is changed are looped through
 * 
 * @param a arc that was inserted/deleted
 * @param operation 
 */
void SubgraphCounts::sFCliqueArcChange(Algora::Arc *a, void(operation)(ska::bytell_hash_map<int, int> &, int, int))
{
	const Algora::Vertex *head = a->getHead();
	const Algora::Vertex *tail = a->getTail();

	for (auto s : this->CurrentObservedVertices)
	{
		operation(sFClique, s->getId(), sFCliqueCount(head, tail, s));
	}
	if (!observedVertices.empty())
	{
		return;
	}


	operation(sFClique, head->getId(), sFCliqueCount(head, tail, head));
	operation(sFClique, tail->getId(), sFCliqueCount(head, tail, tail)); 
    graph->getDiGraph()->mapIncidentArcs(head,[&](const Algora::Arc *a1)
    {
        auto s = a1->getOther(head);
		if (s == tail)
			return;
		operation(sFClique, s->getId(), sFCliqueCount(head, tail, s)); 
	});
}
