# Subgraph Counting Dynamic Algorithm

This program is designed to count size four subgraphs in dynamic setting. There is an example application provided. The program is based on the modular algorithm framework
[**Algora**](https://libalgora.gitlab.io). The theoretical description can be found [here](https://drops.dagstuhl.de/opus/volltexte/2022/15960/pdf/LIPIcs-SAND-2022-18.pdf)[^1]

[^1]: Hanauer, K., Henzinger, M., and Hua, Q. C. Fully dynamic four-vertex subgraph counting. In 1st Symposium on Algorithmic Foundations of Dynamic Networks, SAND 2022, March 28-30, 2022, Virtual Conference (2022), J. Aspnes and O. Michail, Eds., vol. 221 of LIPIcs, Schloss Dagstuhl - Leibniz-Zentrum für Informatik, pp. 18:1–18:17.

The program also makes use of the static subgraph algorithm oaqc [^2] and the hash map implementation of Malte Skarupke [^3].

[^2]: Ortmann, M., and Brandes, U. Efficient orbit-aware triad and quad census in directed and undirected graphs. Applied network science 2, 1 (2017),1–17.

[^3]: Skarupke, M. Flat hash map. https://github.com/Skarupke/flat_hash_map, 2017.

## Building
To use the algorithm, it is neccersary to first compile the Algora framework. This includes [**AlgoraCore**](https://gitlab.com/AlgoRhythmics/AlgoraCore) and [**AlgoraDyn**](https://gitlab.com/AlgoRhythmics/AlgoraDyn). This program was tested using the "develop" branch of the Algora framework. For installation, see the links above.

After this is completed, the repository can be cloned and the example program compiled using the following commands:

```
$ cd Algora
$ git clone https://gitlab.com/leonhards98/subgraph-counting
$ cd subgraph_counting
$ ./easycompile
```

The compiled binaries can then be found in the `build/Debug` and `build/Release`
subdirectories.

## Usage
The example program shows the basic usage of the program.

After a `SubgraphCounting`-Object is created, a dynamicDiGraph from the Algora framework needs to be provided. Then two values for the parameter `epsilon` need to be provided. The first is used for all subgraphs where the theoretical optimum is 0.33, the second for all subgraphs where the theoretical optimum is 0.5. 

Then, the objectives need to be set. This includes wich subgraphs to count and for what vertices. After the object is prepared, its member functions provide all current counts. They are not saved automatically.

The procedure described above looks as follows:

    SubCounts.setGraph(&dyGraph);
	SubCounts.setEpsilon(epsilon1, epsilon2);
	SubCounts.setObjectives(...);
	SubCounts.prepare();

    //Apply the first operation of the dynamic graph
    dyGraph.applyNextOperation();

    //Get total paw-count
    int nrPaws = SubCounts.getNrPaws(); 

    //Get s-count for paws with vertex v
    int nrPawsS = SubCounts.getNrPaws(v)

To run the program, the following command can be used:

    $ ./build/Release/AlgoraApp -f tests/test2.txt -o time.txt  -a -s -e 0.33,0.5

This counts all (-a) available s-counts (-s) of the graph tests/test2.txt (-f) using epsilons 0.33 and 0.5 (-e). 
The time measurements are written into the file time.txt
