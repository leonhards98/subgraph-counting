#include "Graph.h"
#include "QuadCensus.h"
#include <iostream>


int main()
{
	int n = 4;
	int m = 4;
	int *edegs_array = new int[n*2] {0,0,1,0,1,2,2,3};
	const int* const edges = edegs_array;

	oaqc::QuadCensus* quad = new oaqc::QuadCensus(n,m,edges);

	auto nCount = quad->getNOrbitCount();
	
	const unsigned long* nOrbit = new unsigned long[nCount];
	const unsigned int* mapping = new unsigned int[n];

	mapping = quad->getMapping();
	nOrbit = quad->nOrbits();
	

	for (int i = 1; i <= nCount; i++)
		std::cout << i << " ";
	std::cout << "\n";
	for (int r = 0; r < n; r++)
	{
		for (int i = 0; i < nCount; i++)
			std::cout << nOrbit[mapping[r]*nCount+i] << " ";
		std::cout << "\n";
	}





}
